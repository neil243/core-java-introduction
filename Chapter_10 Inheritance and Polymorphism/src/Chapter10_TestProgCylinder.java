//********************************************************************
// Author: Neil Kasanda
//
// Program: Chapter 10, Programming Problem 5
// testing the various operations of a cylinder
//********************************************************************

public class Chapter10_TestProgCylinder 
{
	public static void main(String[] args) 
	{
		Chapter10_ProgProblem5_Cylinder cylinder = 
				new Chapter10_ProgProblem5_Cylinder();
		
		System.out.println("Line 7: cylinder: \n" + cylinder);
		System.out.println();
		
		cylinder.setCoordinates(5, 5);
		System.out.println("Line 10: After setting the center of the base..."); 
		System.out.println("Line 11: cylinder: \n" + cylinder);
		System.out.println();
		
		cylinder.setRadius(13);
		System.out.println("Line 14: After setting the radius of the base...");
		System.out.println("Line 15: cylinder: \n" + cylinder);
		System.out.println();
		
		cylinder.setHeight(17);
		System.out.println("Line 18: After setting the height of the cylinder...");
		System.out.println("Line 19: cylinder: \n" + cylinder);
		System.out.println();
		
		cylinder.setDimensions(3, 7, 9, 12);
		System.out.println("Line 22: After setting all the dimensions "
				+ "of the cylinder...");
		System.out.println("Line 24: cylinder: \n" + cylinder);
		System.out.println();
	}
}