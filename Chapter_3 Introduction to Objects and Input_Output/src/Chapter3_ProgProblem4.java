//********************************************************
// Author: Neil Kasanda
//
// Problem:
// During each summer John and Jessica grow vegetables in their back yard and
// buy seeds and fertilizer from a local nursery. The nursery carries different
// types of vegetable fertilizers in various bag sizes. When buying a particular
// fertilizer, they want to know the price of the fertilizer per pound and the cost
// of fertilizing per square foot. The following program prompts the user to
// enter the size of the fertilizer bag, in pounds, the cost of the bag, and the
// area, in square feet, that can be covered by the bag. The program should
// output the desired result. However, the program contains logic errors. Find
// and correct the logic errors so that the program works properly.
//********************************************************
import java.util.*;

public class Chapter3_ProgProblem4 
{
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		double bagSize;
		double cost;
		double area;
		
		System.out.print("Enter the amount of fertilizer, "
				+ "in pounds, in one bag: ");
		bagSize = console.nextDouble();
		System.out.println();
		
		System.out.print("Enter the cost of the " + bagSize
				+ " pound fertilizer bag: ");
		cost = console.nextDouble();
		System.out.println();
		
		System.out.print("Enter the area, in square feet, that "
				+ "can be fertilized by one bag: ");
		area = console.nextDouble();
		System.out.println();
		
		System.out.printf("The cost of the fertilizer per pound is: "
				+ "$%.2f%n", cost / bagSize);
		System.out.printf("The cost of fertilizing per square "
				+ "foot is: $%.4f%n", cost / area);
	}
}
