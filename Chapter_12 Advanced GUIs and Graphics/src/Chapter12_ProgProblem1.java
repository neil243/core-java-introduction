//*****************************************************************
// Author: Neil Kasanda
// 
// Program: Chapter 12, Programming Problem 1
// Create an applet to draw a digit using the method fillRect of 
// the class Graphics. For instance, if the input is 4, the applet
// will display the digit 4, as shown in Figure 12-27. 
//*****************************************************************

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Chapter12_ProgProblem1 extends JApplet implements KeyListener
{
	private char input;
	
	public void init()
	{
		input = '*';
		
		addKeyListener(this);
		setFocusable(true);	// to allow the window to generate key events
	}
	
	public void paint(Graphics g)
	{
		super.paint(g);
		
		drawDigit(g);
	}
	
	public void keyTyped(KeyEvent event)
	{
		input = event.getKeyChar();
		repaint();
	}
	
	public void keyPressed(KeyEvent event) {}	
	public void keyReleased(KeyEvent event) {}
	
	public void drawDigit(Graphics g)
	{		
		switch(input)
		{
		case '0':
			draw0(g);
			break;
		case '1':
			draw1(g);
			break;
		case '2':
			draw2(g);
			break;
		case '3':
			draw3(g);
			break;
		case '4':
			draw4(g);
			break;
		case '5':
			draw5(g);
			break;
		case '6':
			draw6(g);
			break;
		case '7':
			draw7(g);
			break;
		case '8':
			draw8(g);
			break;
		case '9':
			draw9(g);
			break;
		default:
			g.fillRect(50, 80, 100, 20);	// middle line
		}
	}
	
	public void draw0(Graphics g)
	{
		g.fillRect(50, 0, 20, 90);	// top left line
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(130, 90, 20, 90); // bottom right line
		g.fillRect(50, 160, 100, 20);	// bottom line
		g.fillRect(50, 90, 20, 90);		// bottom left line
	}
	
	public void draw1(Graphics g)
	{
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(130, 90, 20, 90); // bottom right line
	}
	
	public void draw2(Graphics g)
	{
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(50, 80, 100, 20);	// middle line
		g.fillRect(50, 90, 20, 90);		// bottom left line
		g.fillRect(50, 160, 100, 20);	// bottom line
	}
	
	public void draw3(Graphics g)
	{
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(50, 80, 100, 20);	// middle line
		g.fillRect(130, 90, 20, 90); // bottom right line
		g.fillRect(50, 160, 100, 20);	// bottom line
	}
	
	public void draw4(Graphics g)
	{
		g.fillRect(50, 0, 20, 90);	// top left line
		g.fillRect(50, 80, 100, 20);	// middle line
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(130, 90, 20, 90); // bottom right line
	}
	
	public void draw5(Graphics g)
	{
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(50, 0, 20, 90);	// top left line
		g.fillRect(50, 80, 100, 20);	// middle line
		g.fillRect(130, 90, 20, 90); // bottom right line
		g.fillRect(50, 160, 100, 20);	// bottom line
	}
	
	public void draw6(Graphics g)
	{
		g.fillRect(50, 80, 100, 20);	// middle line
		g.fillRect(50, 0, 20, 90);	// top left line
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(130, 90, 20, 90); // bottom right line
		g.fillRect(50, 160, 100, 20);	// bottom line
		g.fillRect(50, 90, 20, 90);		// bottom left line
	}
	
	public void draw7(Graphics g)
	{
		g.fillRect(50, 0, 20, 90);	// top left line
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(130, 90, 20, 90); // bottom right line
	}
	
	public void draw8(Graphics g)
	{
		g.fillRect(50, 80, 100, 20);	// middle line
		g.fillRect(50, 0, 20, 90);	// top left line
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(130, 90, 20, 90); // bottom right line
		g.fillRect(50, 160, 100, 20);	// bottom line
		g.fillRect(50, 90, 20, 90);		// bottom left line
	}
	
	public void draw9(Graphics g)
	{
		g.fillRect(50, 80, 100, 20);	// middle line
		g.fillRect(50, 0, 20, 90);	// top left line
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(130, 90, 20, 90); // bottom right line
		g.fillRect(50, 160, 100, 20);	// bottom line
	}
}
