//********************************************************************
// Author: Neil Kasanda
//
// Program:
// In a right triangle, the square of the length of one side is equal to the sum of
// the squares of the lengths of the other two sides. Write a program that
// prompts the user to enter the lengths of three sides of a triangle and then
// outputs a message indicating whether the triangle is a right triangle.
//********************************************************************

import java.util.*;

public class Chapter4_ProgProblem6 
{
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		double length1;
		double length2;
		double length3;
		
		System.out.print("Enter the lengths of three sides of a right triangle: ");
		length1 = console.nextDouble();
		length2 = console.nextDouble();
		length3 = console.nextDouble();
		System.out.println();
		
		if( (length1 * length1 == length2 * length2 + length3 * length3) ||
			(length2 * length2 == length1 * length1 + length3 * length3) ||
			(length3 * length3 == length1 * length1 + length2 * length2) )
			System.out.println("Right triangle.");
		else
			System.out.println("Not a right triangle");
	}
}
