//********************************************************************
// Author: Neil Kasanda
//
// Program to test the decrement operations of the class
// Clock_Chapter8_ProgProblem1
//********************************************************************

public class TestProgClock_Chapter8_ProgProblem1 
{
	public static void main(String[] args) 
	{
		Clock_Chapter8_ProgProblem1 myClock = 
				new Clock_Chapter8_ProgProblem1(12, 52, 0);
		Clock_Chapter8_ProgProblem1 yourClock = 
				new Clock_Chapter8_ProgProblem1(11, 0, 53);
		
		System.out.print("Line 3: myClock: ");
		myClock.printTime();
		System.out.println();
		
		myClock.decrementSeconds();
		System.out.print("Line 7: after decrementing seconds, myClock: ");
		myClock.printTime();
		System.out.println("\n");
		
		System.out.print("Line 10: yourClock: ");
		yourClock.printTime();
		System.out.println();
		
		yourClock.decrementMinutes();
		System.out.print("Line 14: after decrementing minutes, yourClock: ");
		yourClock.printTime();
		System.out.println();
		
		yourClock.decrementHours();
		System.out.print("Line 18: after decrementing hours, yourClock: ");
		yourClock.printTime();
		System.out.println("\n");
		
		myClock.setTime(0, 0, 0);
		System.out.print("Line 22: after resetting the time, myClock: ");
		myClock.printTime();
		System.out.println();
		
		myClock.decrementSeconds();
		System.out.print("Line 26: after decrementing seconds, myClock: ");
		myClock.printTime();
		System.out.println();
		
		myClock.incrementSeconds();
		System.out.print("Line 26: after incrementing seconds, myClock: ");
		myClock.printTime();
		System.out.println();
	}
}
