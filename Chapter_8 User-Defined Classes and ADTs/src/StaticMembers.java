//*************************************************************************
// Author: Neil Kasanda
// 
// Program: to illustrate how static members of a class work.
//*************************************************************************

public class StaticMembers 
{
	public static void main(String[] args) 
	{
		Illustrate illusObject1 = new Illustrate(3);
		Illustrate illusObject2 = new Illustrate(5);
		
		Illustrate.incrementY();
		Illustrate.count++;
		
		System.out.println("Line 5: illusObject1: " + illusObject1);
		System.out.println("Line 6: illusObject2: " + illusObject2);
		
		System.out.println("Line 7: ***Increment y using "
				+ "illusObject1***");
		illusObject1.incrementY();
		
		illusObject1.setX(8);
		
		System.out.println("Line 10: illusObject1: " + illusObject1);
		System.out.println("Line 11: illusObject2: " + illusObject2);
		
		System.out.println("Line 12: ***Increment y using "
				+ "illusObject2***");
		illusObject2.incrementY();
		
		illusObject2.setX(23);
		
		System.out.println("Line 15: illusObject1: " + illusObject1);
		System.out.println("Line 16: illusObject2: " + illusObject2);
	}
}
