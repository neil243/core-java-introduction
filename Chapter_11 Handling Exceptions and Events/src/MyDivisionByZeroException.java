//**********************************************************************
// Author: Neil Kasanda
//
// Program: Creating my own exception class.
//**********************************************************************

public class MyDivisionByZeroException extends Exception 
{
	public MyDivisionByZeroException()
	{
		super("Cannot divide by zero");
	}
	
	public MyDivisionByZeroException(String strMessage)
	{
		super(strMessage);
	}
}
