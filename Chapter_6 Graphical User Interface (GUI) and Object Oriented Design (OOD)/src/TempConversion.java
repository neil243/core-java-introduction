//********************************************************************
// Author: Neil Kasanda
// 
// Java Program to convert the temperature between Celsius and
// Fahrenheit.
//********************************************************************

import javax.swing.*; // for JFrame, JLabel, and JTextField classes
import java.awt.*;		// for Container class
import java.awt.event.*; // for events

public class TempConversion extends JFrame 
{
	private JLabel celsiusL, fahrenheitL;
	private JTextField celsiusTF, fahrenheitTF;
	
	private CelsHandler celsiusHandler;
	private FahrHandler fahrenheitHandler;
	
	private final int WIDTH = 500;
	private final int HEIGHT = 100;
	private final double FTOC = 5.0 / 9.0;
	private final double CTOF = 9.0 / 5.0;
	private final double OFFSET = 32;
	
	public TempConversion()
	{
		setTitle("Temperature Conversion");
		Container c = getContentPane();
		c.setLayout(new GridLayout(1, 4));
		
		celsiusL = new JLabel("Temp in Celsius:", SwingConstants.RIGHT);
		fahrenheitL = new JLabel("Temp in Fahrenheit:", SwingConstants.RIGHT);
		
		celsiusTF = new JTextField(7);
		fahrenheitTF = new JTextField(7);
		
		c.add(celsiusL);
		c.add(celsiusTF);
		c.add(fahrenheitL);
		c.add(fahrenheitTF);
		
		celsiusHandler = new CelsHandler();
		fahrenheitHandler = new FahrHandler();
		
		celsiusTF.addActionListener(celsiusHandler);
		fahrenheitTF.addActionListener(fahrenheitHandler);
		
		setSize(WIDTH, HEIGHT);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	private class CelsHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			double celsius, fahrenheit;
			
			celsius = Double.parseDouble(celsiusTF.getText());
			
			fahrenheit = celsius * CTOF + OFFSET;
			
			fahrenheitTF.setText(String.format("%.2f", fahrenheit));
		}
	}
	
	private class FahrHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			double celsius, fahrenheit;
			
			fahrenheit = Double.parseDouble(fahrenheitTF.getText());
			
			celsius = (fahrenheit - OFFSET) * FTOC;
			
			celsiusTF.setText(String.format("%.2f", celsius));
		}
	}
	
	public static void main(String[] args) 
	{
		TempConversion tempConv = new TempConversion();
	}
}