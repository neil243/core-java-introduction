//*****************************************************************
// Author: Neil Kasanda
// 
// Program:
// GrandWelcome Applet
//*****************************************************************

import java.awt.*;	// for Color and Font classes
import javax.swing.*;	// for JApplet

public class GrandWelcome extends JApplet 
{
	public void paint(Graphics g)
	{
		super.paint(g);
		
		g.setColor(Color.red);
		g.setFont(new Font("Courrier", Font.BOLD, 24));
		g.drawString("Welcome to Java Programming", 30, 30);
	}
}
