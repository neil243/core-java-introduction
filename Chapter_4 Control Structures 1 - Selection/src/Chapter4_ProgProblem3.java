//*********************************************************************
// Author: Neil Kasanda
//
// Program:
// Write a program that prompts the user to input an integer between 0 and
// 35. If the number is less than or equal to 9, the program should output the
// number; otherwise, it should output A for 10, B for 11, C for 12, . . ., and Z
// for 35. (Hint: Use the cast operator, (char)( ), for numbers >= 10.)
//*********************************************************************

import java.util.*;

public class Chapter4_ProgProblem3 
{
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		int number;
		
		System.out.print("Enter a number between 0 and 35: ");
		number = console.nextInt();
		System.out.println();
		
		if(number <= 9)
			System.out.println("Output: " + number);
		else
			System.out.println("Output: " + (char)(number + 55));
	}
}
