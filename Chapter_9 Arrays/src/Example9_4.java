//*********************************************************************
// Author: Neil Kasanda
//
// Program:
// read five integers, find their sum and print said integers in reverse
// order.
//*********************************************************************

import java.util.*;

public class Example9_4 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		int[] items = new int[5];
		int sum;
		
		System.out.print("Enter five integers: ");
		
		sum = 0;
		for(int counter = 0; counter < items.length; counter++)
		{
			items[counter] = console.nextInt();
			sum = sum + items[counter];
		}
		
		System.out.println("The sum of the numbers = " + sum);
		
		System.out.print("The numbers in reverse order are: ");
		
			// print the numbers in the reverse order
		for(int counter = items.length - 1; counter >= 0; counter--)
			System.out.print(items[counter] + " ");
		
		System.out.println();
	}
}
