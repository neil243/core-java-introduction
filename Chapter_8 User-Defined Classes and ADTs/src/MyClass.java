//*************************************************************************
// Author: Neil Kasanda
//
// Program:
// Write the definition of the class according to the descriptions
//*************************************************************************

public class MyClass 
{
	private int x;
	private static int count;
	
		// Default constructor
		// Postcondition: x = 0;
	public MyClass()
	{
		setX(0);
	}
	
		// Constructor with a parameter
		// Postcondition: x = a;
	public MyClass(int a)
	{
		setX(a);
	}
	
		// Method to set the value of x
		// Postcondition: x = a;
	public void setX(int a)
	{
		x = a;
	}
	
		// Method to output x.
	public void printX()
	{
		System.out.println(x);
	}
	
		// Method to output count
	public static void printCount()
	{
		System.out.println(count);
	}
	
		// Method to increment count
		// Postcondition: count++;
	public static int incrementCount()
	{
		count++;
		return count;
	}
}
