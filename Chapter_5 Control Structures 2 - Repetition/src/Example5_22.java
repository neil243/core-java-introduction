//**********************************************************************
// Author: Neil Kasanda
//
// Program: 
// Assume that the data below describes certain candidates seeking the 
// student councilís presidential seat.
// 101
// Lance Smith
// 65 78 65 89 25 98 -999
// 102
// Cynthia Marker
// 87 34 89 99 26 78 64 34 -999
// 103
// Sheila Mann
// 23 99 98 97 26 78 100 63 87 23 -999
// 104
// David Arora
// 62 35 78 99 12 93 19 -999
// ...
// 
// For each candidate the data is in the following form:
// ID
// Name
// Votes
// 
// The objective is to find the total number of votes received by each 
// candidate.
//**********************************************************************

import java.util.*;
import java.io.*;

public class Example5_22 
{
	public static void main(String[] args) throws FileNotFoundException
	{
		Scanner inFile = new Scanner(new FileReader("Exp_5_22.txt"));
		
		int ID;
		int num;
		int sum;
		String name;
		
		while(inFile.hasNext())
		{
			ID = inFile.nextInt();
			inFile.nextLine();	// discard the newline character after the ID
			name = inFile.nextLine();
			
			sum = 0;
			
			num = inFile.nextInt();
			
			while(num != -999)
			{
				sum = sum + num;
				num = inFile.nextInt();
			}
			
			System.out.println("Name: " + name + ", Votes: " + sum);
		}
		
		inFile.close();
	}
}
