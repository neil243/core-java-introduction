//*************************************************************
// Author: Neil Kasanda
// 
// Program:
// Redo Exercise 9 to handle floating-point numbers. (Format your output to
// two decimal places.)
//*************************************************************

import javax.swing.*;

public class Chapter4_ProgProblem10 
{
	public static void main(String[] args) 
	{
		double num1;
		double num2;
		String inputStr;
		String outputStr;
		
		inputStr = JOptionPane.showInputDialog("Enter the first number");
		num1 = Double.parseDouble(inputStr);
		
		inputStr = JOptionPane.showInputDialog("Enter the second number");
		num2 = Double.parseDouble(inputStr);
		
		inputStr = JOptionPane.showInputDialog("Enter the operation to perform");
		
		switch(inputStr.charAt(0))
		{
		case '+':
			outputStr = num1 + " + " + num2 + " = " + String.format("%.2f %n", (num1 + num2));
			JOptionPane.showMessageDialog(null, outputStr, 
					"Simple Calculator", JOptionPane.INFORMATION_MESSAGE);
			break;
		
		case '-':
			outputStr = num1 + " - " + num2 + " = " + String.format("%.2f %n", (num1 - num2));
			JOptionPane.showMessageDialog(null, outputStr, 
					"Simple Calculator", JOptionPane.INFORMATION_MESSAGE);
			break;
		
		case '/':
			if(num2 == 0)
			{
				JOptionPane.showMessageDialog(null, "Denominator must be nonzero", 
						"Simple Calculator", JOptionPane.INFORMATION_MESSAGE);
				break;
			}
			
			outputStr = num1 + " / " + num2 + " = " + String.format("%.2f %n", (num1 / num2));
			JOptionPane.showMessageDialog(null, outputStr, 
					"Simple Calculator", JOptionPane.INFORMATION_MESSAGE);
			break;
		
		case '*':
			outputStr = num1 + " * " + num2 + " = " + String.format("%.2f %n", (num1 * num2));
			JOptionPane.showMessageDialog(null, outputStr, 
					"Simple Calculator", JOptionPane.INFORMATION_MESSAGE);
			break;
		
		default:
			JOptionPane.showMessageDialog(null, "Operation \"" + inputStr + "\" not supported", 
					"Simple Calculator", JOptionPane.INFORMATION_MESSAGE);
		}
		
		System.exit(0);
	}
}