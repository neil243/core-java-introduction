//******************************************************************
// Author: Neil Kasanda
//
// Program: Code Detection
// This program checks whether the message received at the
// destination is error-free. If there is an error in the
// message, then the program outputs an error message and asks
// for retransmission.
//******************************************************************

import java.util.*;

public class CodeDetection 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		int codeLength;
		
		System.out.print("Enter the length of the code: ");
		codeLength = console.nextInt();
		System.out.println();
		
		int[] codeArray = new int[codeLength];
		
		readCode(codeArray);
		compareCode(codeArray);
		
		console.close();
	}
	
	public static void readCode(int[] list)
	{
		System.out.print("Enter the secret code: ");
		for(int count = 0; count < list.length; count++)
		{
			list[count] = console.nextInt();
		}
		System.out.println();
	}
	
	public static void compareCode(int[] list)
	{
		int length2;
		int digit;
		boolean codeOk;
		
		codeOk = true;
		
		System.out.print("Enter the length of the copy of the secret code "
				+ "\nand a copy of the secret code: ");
		length2 = console.nextInt();
		System.out.println();
		
		if(list.length != length2)
		{
			System.out.println("The original code and its copy are not "
					+ "of the same length.");
			return;
		}
		
		System.out.println("Code Digit    Code Digit Copy");
		
		for(int count = 0; count < list.length; count++)
		{
			digit = console.nextInt();
			
			System.out.printf("%5d %15d", list[count], digit);
			
			if(list[count] != digit)
			{
				System.out.println("  corresponding code digits not the same");
				codeOk = false;
			}
			else
				System.out.println();
		}
		
		if(codeOk)
			System.out.println("Message transmitted OK.");
		else
			System.out.println("Error in transmission. Retransmit!!");
	}
}
