//**************************************************************
// Author: Neil Kasanda
//
// Problem:
// Consider the following program in which the statements are in the incorrect
// order. Rearrange the statements so that the program prompts the user to
// input the height and the radius of the base of a cylinder, and outputs the
// volume and surface area of the cylinder. Also modify the relevant output
// statements to format the output to two decimal places.
//**************************************************************
import java.util.*;

public class Chapter3_ProgProblem2 
{
	static Scanner console = new Scanner(System.in);
	
	static final double PI = 3.14159;
	
	public static void main(String[] args) 
	{
		double height;
		double radius;
		
		System.out.print("Enter the height of the cylinder: ");
		height = console.nextDouble();
		System.out.println();
		
		System.out.print("Enter the radius of the base of the cylinder: ");
		radius = console.nextDouble();
		System.out.println();
		
		System.out.println("Volume of the cylinder = "
				+ String.format("%.2f", PI * Math.pow(radius, 2.0) * height));
		
		System.out.printf("Surface area: %.2f", 
				(2 * PI * Math.pow(radius, 2.0)) + (2 * PI * radius * height));
	}
}
