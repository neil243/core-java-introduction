//**********************************************************
// Author: Neil Kasanda
// 
// Program: 
// Consider the following Java program in which the statements are in the
// incorrect order. Rearrange the statements so that it prompts the user to input
// the length and width of a rectangle and output the area and perimeter of the
// rectangle.
//**********************************************************

import java.util.*;

public class Chapter2_ProgProblem5 
{
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		int length;
		int width;
		int area;
		int perimeter;
		
		System.out.print("Enter the length: ");
		length = console.nextInt();
		System.out.println();
		
		System.out.print("Enter the width: ");
		width = console.nextInt();
		System.out.println();
		
		area = length * width;
		System.out.println("Area = " + area);
		
		perimeter = 2 * (length + width);
		System.out.println("Perimeter = " + perimeter);
	}
}
