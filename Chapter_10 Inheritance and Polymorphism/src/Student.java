//***********************************************************************
// Author: Neil Kasanda
//
// Program:
// Class to design and implement the properties of a student.
// This class is used by the class GradeReportProgram
//***********************************************************************
public class Student extends Person 
{
	private int sId;
	private int numberOfCourses;
	private boolean isTuitionPaid;
	private Course[] coursesEnrolled;
	private char[] courseGrades;
	
	public Student()
	{
		super();
		sId = 0;
		numberOfCourses = 0;
		isTuitionPaid = false;
		
		coursesEnrolled = new Course[6];
		
		for(int i = 0; i < 6; i++)
			coursesEnrolled[i] = new Course();
		
		courseGrades = new char[6];
		
		for(int i = 0; i < 6; i++)
			courseGrades[i] = '*';
	}
	
	public void setInfo(String fName, String lName, int ID, 
			int nOfCourses, boolean isTPaid, Course[] courses, char[] cGrades)
	{
		super.setName(fName, lName);
		sId = ID;
		numberOfCourses = nOfCourses;
		isTuitionPaid = isTPaid;
		
		for(int i = 0; i < numberOfCourses; i++)
		{
			coursesEnrolled[i].copyCourseInfo(courses[i]);
			courseGrades[i] = cGrades[i];
		}
		
		sortCourses();
	}
	
	public void setStudentId(int ID)
	{
		sId = ID;
	}
	
	public void setIsTuitionPaid(boolean isTPaid)
	{
		isTuitionPaid = isTPaid;
	}
	
	public void setNumberOfCourses(int nOfCourses)
	{
		numberOfCourses = nOfCourses;
	}
	
	public void setCoursesEnrolled(Course[] courses, char[] cGrades)
	{
		for(int i = 0; i < numberOfCourses; i++)
		{
			coursesEnrolled[i].copyCourseInfo(courses[i]);
			courseGrades[i] = cGrades[i];
		}
		
		sortCourses();
	}
	
	public String toString()
	{
		String gReport;
		
		gReport = "Student Name: " + super.toString() + "\r\n"
				+ "Student ID: " + sId + "\r\n" 
				+ "Number of courses enrolled: " + numberOfCourses + "\r\n"
				+ String.format("%-12s%-15s%-8s%-6s%n", "Course No", 
						"Course Name", "Credits", "Grade");
		
		for(int i = 0; i < numberOfCourses; i++)
		{
			gReport = gReport + coursesEnrolled[i];
			
			if(isTuitionPaid)
				gReport = gReport + String.format("%8s%n", courseGrades[i]);
			else
				gReport = gReport + String.format("%8s%n", "***");
		}
		
		gReport = gReport + "\r\nTotal number of credit hours: " + getHoursEnrolled() + "\r\n";
		
		return gReport;
	}
	
	public int getStudentId()
	{
		return sId;
	}
	
	public boolean getIsTuitionPaid()
	{
		return isTuitionPaid;
	}
	
	public int getNumberOfCourses()
	{
		return numberOfCourses;
	}
	
	public Course getCourse(int i)
	{
		Course temp = new Course();
		
		temp.copyCourseInfo(coursesEnrolled[i]);
		
		return temp;
	}
	
	public char getGrade(int i)
	{
		return courseGrades[i];
	}
	
	public int getHoursEnrolled()
	{
		int totalCredits = 0;
		
		for(int i = 0; i < numberOfCourses; i++)
			totalCredits = totalCredits + coursesEnrolled[i].getCredits();
		
		return totalCredits;
	}
	
	public double getGpa()
	{
		double sum = 0;
		
		for(int i = 0; i < numberOfCourses; i++)
		{
			switch(courseGrades[i])
			{
			case 'A':
				sum = sum + coursesEnrolled[i].getCredits() * 4;
				break;
				
			case 'B':
				sum = sum + coursesEnrolled[i].getCredits() * 3;
				break;
				
			case 'C':
				sum = sum + coursesEnrolled[i].getCredits() * 2;
				break;
				
			case 'D':
				sum = sum + coursesEnrolled[i].getCredits() * 1;
				break;
				
			case 'F':
				break;
				
			default:
				System.out.println("Invalid Course Grade.");
			}
		}
		
		return sum / getHoursEnrolled();
	}
	
	public double billingAmount(double tuitionRate)
	{
		return tuitionRate * getHoursEnrolled();
	}
	
	private void sortCourses()
	{
		int minIndex;
		Course temp = new Course();	// variable to swap data
		String course1;
		String course2;
		
		char tempGrade;
		
		for(int i = 0; i < numberOfCourses - 1; i++)
		{
			minIndex = i;
			
			for(int j = i + 1; j < numberOfCourses; j++)
			{
					// get course numbers
				course1 = coursesEnrolled[minIndex].getCourseNumber();
				course2 = coursesEnrolled[j].getCourseNumber();
				
				if(course1.compareTo(course2) > 0)
					minIndex = j;
			}//end for
			
			temp = coursesEnrolled[minIndex];
			coursesEnrolled[minIndex] = coursesEnrolled[i];
			coursesEnrolled[i] = temp;
			
			tempGrade = courseGrades[minIndex];
			courseGrades[minIndex] = courseGrades[i];
			courseGrades[i] = tempGrade;
		}//end for
	}//end sortCourses
}
