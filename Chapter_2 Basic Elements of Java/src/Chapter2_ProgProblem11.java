//**************************************************************
// Author: Neil Kasanda
//
// Problem:
// Write a Java program that prompts the user to input the elapsed time for an
// event in seconds. The program then outputs the elapsed time in hours,
// minutes, and seconds. (For example, if the elapsed time is 9630 seconds,
// then the output is 2:40:30.)
//
//**************************************************************

import java.util.*;

public class Chapter2_ProgProblem11 
{
	static Scanner console = new Scanner(System.in);
	
	static final int SECONDS_PER_HOUR = 3600;
	static final int SECONDS_PER_MINUTE = 60;
	
	public static void main(String[] args) 
	{
		int seconds, hours, minutes;
		
		System.out.print("Enter the elapsed time for an event in seconds: ");
		seconds = console.nextInt();
		
			// Calculate the number of hours
		hours = seconds / SECONDS_PER_HOUR;
		
			// Calculate the remaining number of seconds
		seconds = seconds % SECONDS_PER_HOUR;
		
			// Calculate the number of minutes
		minutes = seconds / SECONDS_PER_MINUTE;
		
		// Calculate the remaining number of seconds
		seconds = seconds % SECONDS_PER_MINUTE;
		
		System.out.println("The elapsed time in 'hours:minutes:seconds' is " + 
				hours + ":" + minutes + ":" + seconds);
		
	}
}
