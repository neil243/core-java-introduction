//***********************************************************************
// Author: Neil Kasanda
// 
// Program:
// Chapter 6, Exercise 9
// Modify the program to compute the area and perimeter of a rectangle so
// that your new program will compute the sum and product of two numbers.
//***********************************************************************

import javax.swing.*;		// for JFrame and other GUIs
import java.awt.*;			// for Container class
import java.awt.event.*;	//for events

public class Exercise9_SumAndProductOfTwoNumbers extends JFrame 
{
	public JLabel number1L, number2L, sumL, productL;
	
	public JTextField number1TF, number2TF, sumTF, productTF;
	
	public JButton calculateB, exitB;
	
	public final int WIDTH = 400;
	public final int HEIGHT = 300;
	
	public Exercise9_SumAndProductOfTwoNumbers()
	{
			// Set window title
		setTitle("Sum and Product of Two Numbers");
		
			// Instantiate all GUI objects
		number1L = new JLabel("Enter first number: ", SwingConstants.RIGHT);
		number2L = new JLabel("Enter second number: ", SwingConstants.RIGHT);
		sumL = new JLabel("Sum: ", SwingConstants.RIGHT);
		productL = new JLabel("Product: ", SwingConstants.RIGHT);
		
		number1TF = new JTextField(8);
		number2TF = new JTextField(8);
		sumTF = new JTextField(15);
		productTF = new JTextField(15);
		
		calculateB = new JButton("Calculate");
		exitB = new JButton("Exit");
		
			// Register needed action listeners with respective objects
		calculateB.addActionListener(new CalculateButtonHandler());
		exitB.addActionListener(new ExitButtonHandler());
		
			// Get window pane
		Container pane = getContentPane();
		pane.setLayout(new GridLayout(5, 2));
		
			// Add GUI elements to pane
		pane.add(number1L);
		pane.add(number1TF);
		pane.add(number2L);
		pane.add(number2TF);
		pane.add(sumL);
		pane.add(sumTF);
		pane.add(productL);
		pane.add(productTF);
		pane.add(calculateB);
		pane.add(exitB);
		
			// Finish window setup
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	private class CalculateButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			double num1, num2, sum, product;
			
			num1 = Double.parseDouble(number1TF.getText());
			num2 = Double.parseDouble(number2TF.getText());
			
			sum = num1 + num2;
			product = num1 * num2;
			
			sumTF.setText("" + sum);
			productTF.setText("" + product);
		}
	}
	
	private class ExitButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			System.exit(0);
		}
	}
	public static void main(String[] args) 
	{
		Exercise9_SumAndProductOfTwoNumbers progObject = 
				new Exercise9_SumAndProductOfTwoNumbers();
	}
}
