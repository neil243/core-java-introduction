//************************************************************************
// Author: Neil Kasanda
// 
// Program:
// Modify the RollDice program, Example 7-3, so that it allows the user to
// enter the desired sum of the numbers to be rolled. Also allow the user to call
// the rollDice method as many times as the user desires.
//************************************************************************

import java.util.*;

public class Chapter7_ProgProblem7 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		int sum;
		int count;
		
		System.out.print("Enter the sum (range is 2 to 12) to be rolled (-1 to quit): ");
		sum = console.nextInt();
		System.out.println();
		
		while(sum != -1)
		{
			count = rollDice(sum);
			System.out.println("It took " + count + " dice rolls to get " + sum);
			
			System.out.print("Enter the sum to be rolled (-1 to quit): ");
			sum = console.nextInt();
			System.out.println();
		}
		
		System.out.println("Goodbye!");
	}
	
	public static int rollDice(int num)
	{
		int die1;
		int die2;
		int sum;
		int rollCount = 0;
		
		do
		{
			die1 = (int) (Math.random() * 6) + 1;
			die2 = (int) (Math.random() * 6) + 1;
			sum = die1 + die2;
			rollCount++;
		}
		while(num != sum);
		
		return rollCount;
	}
}
