//***********************************************************
// Author: Neil Kasanda
//
// Problem:
// Write a program that prompts the user to enter five test scores and then
// prints the average test score.
//***********************************************************

import java.util.*;

public class Chapter2_ProgProblem7 
{
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		double score1, score2, score3, score4, score5, average;
		
		System.out.print("Enter five scores separated by spaces: ");
		score1 = console.nextDouble();
		score2 = console.nextDouble();
		score3 = console.nextDouble();
		score4 = console.nextDouble();
		score5 = console.nextDouble();
		//System.out.println();
		
		average = (score1 + score2 + score3 + score4 + score5) / 5;
		System.out.print("The average test score = " + average);
	}
}
