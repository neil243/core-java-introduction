//***************************************************************
// Author: Neil Kasanda
// 
// Program:
// Design and implement a GUI program to compare two strings and display
// the larger one.
//***************************************************************
import javax.swing.*;		// for JFrame and other GUI elements
import java.awt.*;			// for Container class
import java.awt.event.*;	// for events

public class Chapter6_ProgProblem3 extends JFrame
{
	private JLabel string1L, string2L, largerStringL;
	
	private JTextField string1TF, string2TF, largerStringTF;
	
	private JButton processB, exitB;
	
	private final int WIDTH = 400;
	private final int HEIGHT = 300;
	
	public Chapter6_ProgProblem3()
	{
		setTitle("");
		
			// Instantiate all GUIs
				// Labels
		string1L = new JLabel("String one: ", SwingConstants.RIGHT);
		string2L = new JLabel("String two: ", SwingConstants.RIGHT);
		largerStringL = new JLabel("Larger string: ", SwingConstants.RIGHT);
		
				// Text fields
		string1TF = new JTextField(50);
		string2TF = new JTextField(50);
		largerStringTF = new JTextField(50);
		
				// Buttons
		processB = new JButton("Find Larger String");
		exitB = new JButton("Exit");
		
			// Register necessary listeners
		processB.addActionListener(new ProcessButtonHandler());
		exitB.addActionListener(new ExitButtonHandler());
		
			// Get window pane
		Container pane = getContentPane();
		
			// Set pane layout
		pane.setLayout(new GridLayout(4, 2));
		
			// Add GUIs to pane
		pane.add(string1L);
		pane.add(string1TF);
		pane.add(string2L);
		pane.add(string2TF);
		pane.add(largerStringL);
		pane.add(largerStringTF);
		pane.add(processB);
		pane.add(exitB);
		
			// finish window setup
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	private class ProcessButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			String string1, string2;
			
			string1 = string1TF.getText();
			string2 = string2TF.getText();
			
			if(string1.length() == string2.length())
				largerStringTF.setText("Both strings are the same length.");
			
			else if(string1.length() > string2.length())
				largerStringTF.setText(string1);
			
			else
				largerStringTF.setText(string2);
		}
	}
	
	private class ExitButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			System.exit(0);
		}
	}
	
	public static void main(String[] args) 
	{
		Chapter6_ProgProblem3 progObj = new Chapter6_ProgProblem3();
	}
}
