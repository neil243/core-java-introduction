
public class SuperSubClassObjects 
{
	public static void main(String[] args)
	{
		RectangleShape rectangle, rectRef;
		BoxShape box, boxRef;
		
		rectangle = new RectangleShape(12, 4);
		System.out.println("Line 4: Rectangle\n" + rectangle + "\n");
		
		box = new BoxShape(13, 7, 4);
		System.out.println("Line 6: Box\n" + box + "\n");
		
		rectRef = box;
		System.out.println("Line 8: Box via rectRef\n" + rectRef + "\n");
		
		boxRef = (BoxShape) rectRef;
		System.out.println("Line 10: Box via boxRef\n" + boxRef + "\n");
		
		if(rectRef instanceof BoxShape)
			System.out.println("Line 12: rectRef is an instance of BoxShape.");
		else
			System.out.println("Line 14: rectRef is not an instance of BoxShape.");
		
		if(rectangle instanceof BoxShape)
			System.out.println("Line 16: rectangle is an instance of BoxShape.");
		else
			System.out.println("Line 18: rectangle is not an instance of BoxShape.");
	}
}