//***************************************************************
// Author: Neil Kasanda
// 
// Problem:
// The manager of a football stadium wants you to write a program that
// calculates the total ticket sales after each game. There are four types of
// tickets�box, sideline, premium, and general admission. After each game,
// data is stored in a file in the following form:
// ticketPrice numberOfTicketsSold
// .
// .
// .
// Sample data are shown below:
// 250 5750
// 100 28000
// 50 35750
// 25 18750
// The first line indicates that the box ticket price is $250 and that 5750 tickets
// were sold at that price. Output the number of tickets sold and the total sale
// amount. Format your output with two decimal places.
//***************************************************************

import java.util.*;

import javax.swing.JOptionPane;

import java.io.*;

public class Chapter3_ProgProblem5 
{
	public static void main(String[] args) throws FileNotFoundException
	{
		Scanner inFile = new Scanner(new FileReader("ticketsData.txt"));
		
		double boxPrice;
		double sidelinePrice;
		double premiumPrice;
		double admissionPrice;
		double totalSaleAmount;
		
		int noOfBoxTicketsSold;
		int noOfSidelineTicketsSold;
		int noOfPremiumTicketsSold;
		int noOfAdmissionTicketsSold;
		int totalNoOfTicketsSold;
		
		String outStr;
		
		boxPrice = inFile.nextDouble();
		noOfBoxTicketsSold = inFile.nextInt();
		
		sidelinePrice = inFile.nextDouble();
		noOfSidelineTicketsSold = inFile.nextInt();
		
		premiumPrice = inFile.nextDouble();
		noOfPremiumTicketsSold = inFile.nextInt();
		
		admissionPrice = inFile.nextDouble();
		noOfAdmissionTicketsSold = inFile.nextInt();
		
		totalNoOfTicketsSold = noOfBoxTicketsSold + noOfSidelineTicketsSold +
				noOfPremiumTicketsSold + noOfAdmissionTicketsSold;
		
		totalSaleAmount = boxPrice * noOfBoxTicketsSold + 
							sidelinePrice * noOfSidelineTicketsSold +
							premiumPrice * noOfPremiumTicketsSold + 
							admissionPrice * noOfAdmissionTicketsSold;
		
		outStr = String.format("Total Number of Tickets Sold: %d%n"
				+ "Total Sale Amount: $%.2f", totalNoOfTicketsSold, totalSaleAmount);
		
		JOptionPane.showMessageDialog(null, outStr, 
				"Ticket Sales", JOptionPane.INFORMATION_MESSAGE);
		inFile.close();
	}
}
