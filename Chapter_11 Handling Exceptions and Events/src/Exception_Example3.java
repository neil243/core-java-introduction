//**********************************************************************
// Author: Neil Kasanda
//
// Program:
// To illustrate handling of exceptions
//**********************************************************************

import java.util.*;

public class Exception_Example3 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		int dividend, divisor, quotient;
		
		try
		{
			System.out.print("Line 4: Enter the dividend: ");
			dividend = console.nextInt();
			System.out.println();
			
			System.out.print("Line 7: Enter the divisor: ");
			divisor = console.nextInt();
			System.out.println();
			
			quotient = dividend / divisor;
			System.out.println("Line 11: Quotient = " + quotient);
		}
		catch(ArithmeticException aeRef)
		{
			System.out.println("Line 13: Exception " + aeRef.toString());
		}
		catch(InputMismatchException imeRef)
		{
			String str;
			
			str = console.next();
			System.out.println("Line 15: Exception " 
					+ imeRef.toString() + " " + str);
		}
	}
}
