//**********************************************************************
// Author: Neil Kasanda
//
// Program: The following program continues to prompt the user until 
// the user enters a valid integer
//**********************************************************************

import java.util.*;

public class FixErrorAndContinue 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		int number;
		boolean done;
		String str;
		
		done = false;
		
		do
		{
			try
			{
				System.out.print("Line 7: Enter an integer: ");
				number = console.nextInt();
				System.out.println();
				done = true;
				
				System.out.println("Line 11: number = " + number);
			}
			catch(InputMismatchException imeRef)
			{
				str = console.next();
				System.out.println("Line 14: Exception " + imeRef.toString() 
					+ " " + str);
			}
		}
		while(!done);
	}
}
