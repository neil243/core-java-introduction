//*************************************************************************
// Author: Neil Kasanda
// 
// Program:
// Write a program to test the method isPalindrome discussed in Example 7-4.
//*************************************************************************

import java.util.*;

public class Chapter7_ProgProblem8 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		String input;
		
		System.out.print("Enter a string to check is it is a palindrome: ");
		input = console.next();
		System.out.println();
		
		if(isPalindrome(input))
			System.out.println(input + " is a palindrome.");
		else
			System.out.println(input + " is not a palindrome.");
	}
	
	public static boolean isPalindrome(String str)
	{
		int j;
		int len = str.length();
		
		j = len - 1;
		for(int i = 0; i < len / 2; i++)
		{
			if(str.charAt(i) != str.charAt(j))
				return false;
			j--;
		}
		
		return true;
	}
}
