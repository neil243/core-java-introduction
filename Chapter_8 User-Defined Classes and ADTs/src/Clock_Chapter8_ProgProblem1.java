//***********************************************************************
// Author: Neil Kasanda
//
// Program:
// The class Clock given in the chapter only allows the time to be incremented
// by one second, one minute, or one hour. Rewrite the definition of the class
// Clock by including additional members so that time can also be decremented by
// one second, one minute, or one hour. Also write a program to test your class.
//***********************************************************************

public class Clock_Chapter8_ProgProblem1 
{
	private int hr;
	private int min;
	private int sec;
	
		// Default constructor
		// Postcondition: hr = 0; min = 0; sec = 0
	public Clock_Chapter8_ProgProblem1()
	{
		setTime(0, 0, 0);
	}
	
		// Constructor with parameters, to set the time
		// The time is set according to the parameters.
		// Postcondition: hr = hours; min = minutes; 
		// sec = seconds;
	public Clock_Chapter8_ProgProblem1(int hour, int minutes, int seconds)
	{
		setTime(hour, minutes, seconds);
	}
	
		// Copy constructor of the class Clock.
		// Postcondition: hr = otherClock.hr; min = otherClockmin
		//				  sec = otherClock.sec
	public Clock_Chapter8_ProgProblem1(
			Clock_Chapter8_ProgProblem1 otherClock)
	{
		setTime(otherClock.hr, otherClock.min, otherClock.sec);
	}
	
		// Method to set the time
		// The time is set according to the parameters.
		// Postcondition: hr = hours; min = minutes;
		// sec = seconds;
	public void setTime(int hour, int minutes, int seconds)
	{
		if(0 <= hour && hour < 24)
			hr = hour;
		else
			hr = 0;
		
		if(0 <= min && min < 60)
			min = minutes;
		else
			min = 0;
		
		if(0 <= sec && sec < 60)
			sec = seconds;
		else
			sec = 0;
	}
	
		// Method to return the hours
		// Postcondition: the value of hr is returned
	public int getHours()
	{
		return hr;
	}
	
		// Method to return the minutes
		// Postcondition: the value of min is returned
	public int getMinutes()
	{
		return min;
	}
	
		// Method to return the seconds
		// Postcondition: the value of sec is returned
	public int getSeconds()
	{
		return sec;
	}
	
		// Method to print the time
		// Postcondition: Time is printed in the form hh:mm:ss
	public void printTime()
	{
		if(hr < 10)
			System.out.print("0");
		System.out.print(hr + ":");
		
		if(min < 10)
			System.out.print("0");
		System.out.print(min + ":");
		
		if(sec < 10)
			System.out.print("0");
		System.out.print(sec);
	}
	
		// Method to increment the time by one second
		// Postcondition: The time is incremented by one second
		// If the before-increment time is 23:59:59, the time
		// is reset to 00:00:00
	public void incrementSeconds()
	{
		sec++;
		if(sec > 59)
		{
			sec = 0;
			incrementMinutes();
		}
	}
	
		// Method to increment the time by one minute
		// Postcondition: The time is incremented by one minute
		// If the before-increment time is 23:59:53, the time
		// is reset to 00:00:53
	public void incrementMinutes()
	{
		min++;
		if(min > 59)
		{
			min = 0;
			incrementHours();
		}
	}
	
		// Method to increment the time by one hour
		// Postcondition: the time is incremented by one hour
		// If the before-increment time is 23:45:53, the time
		// is reset to 00:45:53
	public void incrementHours()
	{
		hr++;
		
		if(hr > 23)
			hr = 0;
	}
	
		// Method to decrement the time by one second
		// Postcondition: the time is decremented by one second
		// If the before-decrement time is 12:22:00, the time is
		// reset to 12:21:59
	public void decrementSeconds()
	{
		sec--;
		if(sec < 0)
		{
			sec = 59;
			decrementMinutes();
		}
	}
	
		// Method to decrement the time by one minute
		// Postcondition: the time is decremented by one minute
		// If the before-decrementTime is 00:00:12, the time is
		// set to 23:59:12
	public void decrementMinutes()
	{
		min--;
		if(min < 0)
		{
			min = 59;
			decrementHours();
		}
	}
	
		// Method to decrement the time by one hour
		// Postcondition: the time is decremented by one hour
		// If the before-decrement time is 00:12:53, the time is
		// set to 23:12:53
	public void decrementHours()
	{
		hr--;
		if(hr < 0)
		{
			hr = 23;
		}
	}
	
		// Method to compare two times
		// Postcondition: Returns true if this time is equal to
		// otherClock; otherwise returns false
	public boolean equals(Clock_Chapter8_ProgProblem1 otherClock)
	{
		return (hr == otherClock.hr &&
				min == otherClock.min &&
				sec == otherClock.sec);
	}
	
		// Method to copy time
		// Postcondition: The instance variables of otherClock
		//				  copied into the corresponding data
		//				  are members of this time.
		//				  hr = otherClock.hr;
		//	  			  min = otherClock.min;
		//				  sec = otherClock.sec;
	public void makeCopy(Clock_Chapter8_ProgProblem1 otherClock)
	{
		setTime(otherClock.hr, otherClock.min, otherClock.sec);
	}
	
		// Method to return a copy of time
		// Postcondition: A copy of the object is created and
		// 				  a reference of the copy is returned
	public Clock_Chapter8_ProgProblem1 getCopy()
	{
		return new Clock_Chapter8_ProgProblem1(hr, min, sec);
	}
}
