//******************************************************************
// Author: Neil Kasanda
//
// Program:
// Write a program that prompts the user to input three numbers. The
// program should then output the numbers in nondescending order.
//******************************************************************

import java.util.*;

public class Chapter4_ProgProblem2 
{
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		double number1;
		double number2;
		double number3;
		
		System.out.print("Enter three numbers separated by spaces: ");
		number1 = console.nextDouble();
		number2 = console.nextDouble();
		number3 = console.nextDouble();
		System.out.println();
		
		if((number1 <= number2) && (number1 <= number3) )
		{
			if(number2 <= number3)
				System.out.println(number1 + " " + number2 + " " + number3);
			else
				System.out.println(number1 + " " + number3 + " " + number2);
		}
		
		else if (number2 <= number1 && number2 <= number3)
		{
			if(number1 <= number3)
				System.out.println(number2 + " " + number1 + " " + number3);
			else
				System.out.println(number2 + " " + number3 + " " + number1);
		}
		
			// Here number3 is smallest
		else
		{
			if(number1 <= number2)
				System.out.println(number3 + " " + number1 + " " + number2);
			else
				System.out.println(number3 + " " + number2 + " " + number3);
		}
	}
}
