//***********************************************************************
// Author: Neil Kasanda
//
// Program:
// program to test the various functionalities of the class StudentData.
//***********************************************************************

public class Example_9_12_TestProgStudentData 
{
	public static void main(String[] args)
	{
		Example_9_12_StudentData student1 = 
				new Example_9_12_StudentData("John", "Doe", 89, 78, 95, 63, 94);
		
		Example_9_12_StudentData student2 = 
				new Example_9_12_StudentData("Lindsay", "Green", 92, 
						82, 90, 70, 87, 99);
		
		System.out.println(student1);
		System.out.println(student2);
	}
}
