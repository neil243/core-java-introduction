//*************************************************************************
// Author: Neil Kasanda
//
// Program: to test cascading method calls
//*************************************************************************

public class TestProgPerson2 
{
	public static void main(String[] args) 
	{
		Person2 student1 = new Person2("Angela", "Smith");
		
		Person2 student2 = new Person2();
		
		Person2 student3 = new Person2();
		
		System.out.println("Line 4: student1: " + student1);
		
		student2.setFirstName("Shelly").setLastName("Malik");
		System.out.println("Line 6: student2: " + student2);
		
		student3.setFirstName("Chelsea");
		System.out.println("Line 8: student3: " + student3);
		
		student3.setLastName("Tomek");
		System.out.println("Line 10: student3: " + student3);
	}
}
