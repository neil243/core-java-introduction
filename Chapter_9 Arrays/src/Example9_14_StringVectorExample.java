//**************************************************************************
// Author: Neil Kasanda
//
// Program:
// StringVectorExample
//**************************************************************************

import java.util.Vector;

public class Example9_14_StringVectorExample 
{
	public static void main(String[] args)
	{
		Vector<String> stringList = new Vector<String>();
		
		System.out.println("Line 7: Empty stringList?: " + stringList.isEmpty());
		System.out.println("Line 8: Size stringList?: " + stringList.size());
		System.out.println();
		
		stringList.addElement("Spring");
		stringList.addElement("Summer");
		stringList.addElement("Fall");
		stringList.addElement("Winter");
		stringList.addElement("Sunny");
		
		System.out.println("Line 15: **** After adding elements to stringList ****");
		System.out.println("Line 16: Empty stringList?: " + stringList.isEmpty());
		System.out.println("Line 17: Size stringList?: " + stringList.size());
		System.out.println("Line 18: stringList: " + stringList);
		
		System.out.println("Line 19: stringList contains Fall?: " 
				+ stringList.contains("Fall"));
		System.out.println();
		
		stringList.removeElement("Fall");
		stringList.removeElementAt(2);
		System.out.println("Line 23: **** After the remove operations ****");
		System.out.println("Line 24: stringList: " + stringList);
	}
}
