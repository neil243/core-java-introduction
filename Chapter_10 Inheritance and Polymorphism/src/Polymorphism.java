
public class Polymorphism 
{
	public static void main(String[] args)
	{
		RectangleFigure rectangle, shapeRef;
		BoxFigure box;
		
		rectangle = new RectangleFigure(8, 5);
		box = new BoxFigure(10, 7, 3);
		
		shapeRef = rectangle;
		System.out.println("Line 10: Rectangle: \n" + shapeRef);
		System.out.println();
		
		shapeRef = box;
		System.out.println("Line 13: Box:\n" + shapeRef);
		System.out.println();
	}
}
