//************************************************************************
// Author: Neil Kasanda
//
// Program: Exercise 25 Chapter 8
// Write the definition of a class that has the following properties:
// a. The name of the class is Stock.
// b. The class Stock has four instance variables: name of type String,
// 		previousPrice and closingPrice of type double, and
// 		numberOfShares of type int.
// c. The class Stock has the following methods:
// 		toString�to return the data stored in the data members with the
// 			appropriate titles as a string
// 		setName�method to set the name
// 		setPreviousPrice�method to set the previous price of a stock.
// 		(This is the closing price of the previous day.)
// 		setClosingPrice�method to set the closing price of a stock
// 		setNumberOfShare�method to set the number of shares owned by
// 			the stock
// 		getName�value-returning method to return the name
// 		getPreviousPrice�value-returning method to return the previous
// 			price of the stock
// 		getClosingPrice�value-returning method to return the closing
// 			price of the stock
// 		getNumberOfShare�value-returning method to return the number
// 			of shares owned by the stock
// 		percentGain�value-returning method to return the change in the
// 			stock value from the previous closing price and today�s closing price as a
// 			percentage
// 		shareValues�value-returning method to calculate and return the
// 			total values of the shares owned
// 		default constructor�the default value of name is the empty string ""; the
// 			default values of previousPrice, closingPrice, and numberOfShares
// 			are 0.
// 		constructor with parameters�sets the values of the instance variables
// 			name, previousPrice, closingPrice, and numberOfShares to the
// 			values specified by the user
// d. Write the definitions of the methods and constructors of the class
// 		Stock as described in part c.
//************************************************************************
public class Stock_Exercise25 
{
	private String name;
	private double previousPrice;
	private double closingPrice;
	private int numberOfShares;
	
	public String toString()
	{
		return "name: " + name + "\n" +
				"Previous Price: " + previousPrice + "\n" + 
				"Closing Price: " + closingPrice + "\n" + 
				"Number of Shares: " + numberOfShares;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setPreviousPrice(double previousPrice)
	{
		this.previousPrice = previousPrice;
	}
	
	public void setClosingPrice(double closingPrice)
	{
		this.closingPrice = closingPrice;
	}
	
	public void setNumberOfShares(int numberOfShares)
	{
		this.numberOfShares = numberOfShares;
	}
	
	public String getName()
	{
		return name;
	}
	
	public double getPreviousPrice()
	{
		return previousPrice;
	}
	
	public double getClosingPrice()
	{
		return closingPrice;
	}
	
	public int getNumberOfShare()
	{
		return numberOfShares;
	}
	
	public double percentGain()
	{
		return closingPrice / previousPrice;
	}
	
	public int shareValues()
	{
		return getNumberOfShare();
	}
	
	public Stock_Exercise25()
	{
		name = "";
		previousPrice = 0.0;
		closingPrice = 0.0;
		numberOfShares = 0;
		
	}
	
	public Stock_Exercise25(String n, 
			double prePrice, double cloPrice, int noShares)
	{
		name = n;
		previousPrice = prePrice;
		closingPrice = cloPrice;
		numberOfShares = noShares;
	}
}
