//*******************************************************************
// Author: Neil Kasanda
// 
// Program:
// Write a method, reverseDigit, that takes an integer as a parameter and
// returns the number with its digits reversed. For example, the value of
// reverseDigit(12345) is 54321. Also, write a program to test your method.
//*******************************************************************

import java.util.*;

public class Chapter7_ProgProblem6 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		int input;
		
		System.out.print("Enter a (positive) number to reverse "
				+ "its digits (-1 to quit): ");
		input = console.nextInt();
		System.out.println();
		
		while(input != -1)
		{
			System.out.println("input: " + input);
			System.out.println("input reversed: " + reverseDigit(input));
			System.out.println();
			
			System.out.print("Enter a number to reverse its digits (-1 to quit): ");
			input = console.nextInt();
			System.out.println();
		}
	}
	
	public static int reverseDigit(int num)
	{
		int numReversed;
		
		numReversed = num % 10;
		num = num / 10;
		
		while(num > 0)
		{
			numReversed = numReversed * 10 + num % 10;
			
			num = num / 10;
		}
		
		return numReversed;
	}
}
