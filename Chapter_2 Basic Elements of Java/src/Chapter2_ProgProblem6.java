//*********************************************************
// Author: Neil Kasanda
//
// Write a program that prompts the user to input a decimal number and
// outputs the number rounded to the nearest integer.
//*********************************************************

import java.util.*;

public class Chapter2_ProgProblem6 
{
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		double num;
		
		System.out.print("Enter a decimal number to be rounded to the nearest integer: ");
		num = console.nextDouble();
		System.out.println();
		System.out.println(num + " rounded to the nearest integer = " + (int)(num + 0.5));
	}
}
