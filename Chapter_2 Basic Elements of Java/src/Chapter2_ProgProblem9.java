//*************************************************************
// Author: Neil Kasanda
// 
// Write a program that does the following:
// a. Prompts the user to input five decimal numbers
// b. Prints the five decimal numbers
// c. Converts each decimal number to the nearest integer
// d. Adds the five integers
// e. Prints the sum and average of the five integers
//*************************************************************

import java.util.*;

public class Chapter2_ProgProblem9 
{
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		double num1, num2, num3, num4, num5, average;
		int number1, number2, number3, number4, number5, sum;
		
		System.out.print("Enter five decimal decimal numbers: ");
		num1 = console.nextDouble();
		num2 = console.nextDouble();
		num3 = console.nextDouble();
		num4 = console.nextDouble();
		num5 = console.nextDouble();
		System.out.println("The five decimal numbers you entered are: " +
				num1 + " " + num2 + " " + num3 + " " + 
				num4 + " " + num5);
		
		number1 = (int)(num1 + 0.5);
		number2 = (int)(num2 + 0.5);
		number3 = (int)(num3 + 0.5);
		number4 = (int)(num4 + 0.5);
		number5 = (int)(num5 + 0.5);
		System.out.println("The decimal numbers rounded to the nearest integers are: " +
				number1 + " " + number2 + " " + number3 + " " +
				number4 + " " + number5);
		
		sum = number1 + number2 + number3 + number4 + number5;
		average = (number1 + number2 + number3 + number4 + number5) / 5.0;
		
		System.out.println("The sum of the five integers = " + sum);
		System.out.println("The average of the five integers = " + average);
	}
}
