//*****************************************************************
// Author: Neil Kasanda
// 
// Program:
// to illustrate BorderLayout layout manager.
//*****************************************************************

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class TextEditor extends JFrame implements ActionListener
{
	private JMenuBar menuMB = new JMenuBar();	// create the menu bar
	
	private JMenu fileM, editM, optionM;
	private JMenuItem exitI;
	private JMenuItem cutI, copyI, pasteI, selectI;
	private JTextArea pageTA = new JTextArea();
	private String scratchpad = "";
	
	public TextEditor()
	{
		setTitle("Simple Text Editor");
		
		Container pane = getContentPane();
		
		pane.setLayout(new BorderLayout());
		pane.add(pageTA, BorderLayout.CENTER);
		pane.add(new JScrollPane(pageTA));
		pageTA.setLineWrap(true);
		
		setJMenuBar(menuMB);
		setFileMenu();
		setEditMenu();
		setSize(300, 200);
		
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	private void setFileMenu()
	{
		fileM = new JMenu("File");
		menuMB.add(fileM);
		
		exitI = new JMenuItem("Exit");
		exitI.addActionListener(this);
		fileM.add(exitI);
	}
	
	private void setEditMenu()
	{
		editM = new JMenu("Edit");
		menuMB.add(editM);
		
		cutI = new JMenuItem("Cut");
		copyI = new JMenuItem("Copy");
		pasteI = new JMenuItem("Paste");
		selectI = new JMenuItem("Select All");
		
		cutI.addActionListener(this);
		copyI.addActionListener(this);
		pasteI.addActionListener(this);
		selectI.addActionListener(this);
		
		editM.add(cutI);
		editM.add(copyI);
		editM.add(pasteI);
		editM.add(selectI);
		
	}
	
	public void actionPerformed(ActionEvent e)
	{
		JMenuItem mItem = (JMenuItem) e.getSource();
		
		if(mItem == exitI)
			System.exit(0);
		else if(mItem == cutI)
		{
			scratchpad = pageTA.getSelectedText();
			pageTA.replaceRange("", pageTA.getSelectionStart(), 
					pageTA.getSelectionEnd());
		}
		else if(mItem == copyI)
		{
			scratchpad = pageTA.getSelectedText();
		}
		else if(mItem == pasteI)
		{
			pageTA.insert(scratchpad, pageTA.getCaretPosition());
		}
		else if(mItem == selectI)
			pageTA.selectAll();
	}
	
	public static void main(String[] args)
	{
		TextEditor texted = new TextEditor();
	}
}
