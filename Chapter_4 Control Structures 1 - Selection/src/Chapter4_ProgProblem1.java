//********************************************************
// Author: Neil Kasanda
//
// Program:
// Write a program that prompts the user to input a number. The program
// should then output the number and a message saying whether the number is
// positive, negative, or zero.
//********************************************************

import java.util.*;

public class Chapter4_ProgProblem1 
{
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		int number;
		
		System.out.print("Enter a number: ");
		number = console.nextInt();
		System.out.println();
		
		if(number == 0)
		{
			System.out.println("Your input: " + number);
			System.out.println("You entered zero.");
		}
		
		else if (number > 0)
		{
			System.out.println("Your input: " + number);
			System.out.println(number + " is a positive number.");
		}
		
		else
		{
			System.out.println("Your input: " + number);
			System.out.println(number + " is a negative number.");
		}
	}
}
