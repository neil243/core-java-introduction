//**********************************************************************
// Author: Neil Kasanda
//
// Program:
// to test the various operations of the class 
// ExtClock_Chapter10_ProgProblem1
//**********************************************************************
public class Chapter10_TestProgExtClock 
{
	public static void main(String[] args)
	{
		Chapter10_ProgProblem1_ExtClock myClock = 
				new Chapter10_ProgProblem1_ExtClock();
		
		Chapter10_ProgProblem1_ExtClock yourClock = 
				new Chapter10_ProgProblem1_ExtClock(3, 15, 55, "GMT-6");
		
		Chapter10_ProgProblem1_ExtClock theirClock = 
				new Chapter10_ProgProblem1_ExtClock();
		
		Clock otherClock = new Clock();
		
		System.out.println("Line 7: myClock: " + myClock);
		System.out.println("Line 8: yourClock: " + yourClock);
		System.out.println()
		;
		myClock.setTimeZone("UTC-11");
		System.out.println("Line 11: After setting time zone of myClock...");
		System.out.println("Line 12: myClock: " + myClock);
		System.out.println();
		
		System.out.println("Line 14: Setting time of myClock...");
		myClock.setTime(23, 58, 59);
		System.out.println("Line 16: After setting time of myClock, myClock: " 
				+ myClock);
		System.out.println();
		
		System.out.println("Line 19: Incrementing myClock's seconds... ");
		myClock.incrementSeconds();
		System.out.println("Line 21: After incrementing myClock's seconds: " + myClock);
		
		System.out.println("Line 22: Incrementing myClock's minutes... ");
		myClock.incrementMinutes();
		System.out.println("Line 24: After incrementing myClock's minutes: " + myClock);
		
		System.out.println("Line 25: Incrementing myClock's hours... ");
		myClock.incrementHours();
		System.out.println("Line 27: After incrementing myClock's hours: " + myClock);
		System.out.println();
		
		otherClock = yourClock;
		System.out.println("Line 30: yourClock via otherClock (object of superclass Clock): " 
				+ otherClock);
		
		System.out.println("Line 32: myClock via yourClock (via otherClock)");
		myClock = (Chapter10_ProgProblem1_ExtClock) otherClock;
		System.out.println("Line 34: myClock: " + myClock);
		System.out.println();
		
		System.out.println("Line 36: Copying a Clock object into an ExtClock object");
		theirClock.makeCopy(otherClock);	// method from super gets executed
		System.out.println("Line 38: After copying otherClock,theirClock: " + theirClock);
		System.out.println();
		
		System.out.println("Line 40: Incrementing myClock's hours...");
		myClock.incrementHours();
		System.out.println("Line 42: myClock: " + myClock);
		
		theirClock.makeCopy(myClock);
		System.out.println("Line 44: Copying an ExtClock object into an ExtClock object. "
				+ "Here time zone is also copied.");
		System.out.println("Line 46: After copying myClockClock, theirClock: " + theirClock);
		System.out.println();
		
		System.out.println("Line 48: Updating Clock object otherClock");
		otherClock.incrementHours();
		otherClock.incrementMinutes();
		otherClock.incrementSeconds();
		System.out.println();
		
		System.out.println("Line 53: Copying Clock object into an ExtClock object again");
		System.out.println("Line 54: Since ExtClock object already has timeZone set, "
				+ "it gets printed... ");
		theirClock.makeCopy(otherClock);	// method from super gets executed
		System.out.println("Line 57: After Copying otherClock into theirClock: " + theirClock);
		System.out.println();
		
	}
}
