//**********************************************************************
// Author: Neil Kasanda
//
// Program: Chapter 8 Programming problem 2
// Write a program that converts a number entered in Roman numerals to
// 	decimal. Your program should consist of a class, say, Roman. An object of
// 	type Roman should do the following:
// a. Store the number as a Roman numeral.
// b. Convert and store the number into decimal.
// c. Print the number as a Roman numeral or decimal number as requested
// 		by the user.
// 		The decimal values of the Roman numerals are:
// 		M 1000
// 		D 500
// 		C 100
// 		L 50
// 		X 10
// 		V 5
// 		I 1
// d. Your class must contain the method romanToDecimal to convert a
// 		Roman numeral into its equivalent decimal number
// e. Test your program using the following Roman numerals: MCXIV,
// 		CCCLIX, and MDCLXVI.
//**********************************************************************

import java.util.*;

public class Roman 
{
	private String romanNotation;
	private int integerNotation;
	
	public Roman()
	{
		romanNotation = "";
	}
	
	public Roman(String roman)
	{
		setRoman(roman);
	}
	
	public void setRoman(String roman)
	{
		romanNotation = roman;
	}
	
	public int romanToDecimal()
	{
		int current = 0;
		int previous = 0;
		integerNotation = 0;
		
		int idx = romanNotation.length() - 1;
		
		for(int i = idx; i >= 0; i--)
		{
			previous = current;

			switch(romanNotation.charAt(i))
			{
			case 'M':
				current = 1000;
				break;
				
			case 'D':
				current = 500;
				break;
				
			case 'C':
				current = 100;
				break;
				
			case 'L':
				current = 50;
				break;
				
			case 'X':
				current = 10;
				break;
				
			case 'V':
				current = 5;
				break;
				
			case 'I':
				current = 1;
				break;
			}
			
			if(current < previous)
				integerNotation = integerNotation - current;
			else
				integerNotation = integerNotation + current;
		}
		
		return integerNotation;
	}
	
	public String toString()
	{
		return romanNotation;
	}
	
	public void printDecimal()
	{
		System.out.println("Decimal value: " + integerNotation);
	}
	
	public void printRoman()
	{
		System.out.println("Roman value: " + romanNotation);
	}
	
	public static void main(String[] args) 
	{
		Scanner console = new Scanner(System.in);
		
		Roman roman = new Roman();
		
		System.out.println("Line 2: roman: " + roman);
		System.out.println();
		
		System.out.print("Line 4: Enter a roman numeral: ");
		roman.setRoman(console.next().toUpperCase());
		System.out.println();
		
		System.out.println("Line 7: roman: " + roman);
		System.out.println("Line 8: " + roman + " to decimal: " + roman.romanToDecimal());
		System.out.println();
		
		roman.setRoman("MCMLXXXIV");
		roman.printRoman();
		roman.romanToDecimal();
		roman.printDecimal();
		
		console.close();
	}
}
