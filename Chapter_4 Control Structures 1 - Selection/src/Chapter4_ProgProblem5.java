//********************************************************************
// Author: Neil Kasanda
//
// Program:
// Write a program to implement the algorithm you designed in Exercise 17 of
// Chapter 1.
//********************************************************************

import java.util.*;

public class Chapter4_ProgProblem5 
{
	static Scanner console = new Scanner(System.in);
	
	static final double SERVICE_CHARGES = 3.00;
	static final double BASIC_PAGE_FEE = 0.20;
	static final double ADDITIONAL_PAGE_FEE = 0.10;
	
	public static void main(String[] args)
	{
		int noOfPagesToBeFaxed;
		double cost;
		
		System.out.print("Enter the number of pages to be faxed: ");
		noOfPagesToBeFaxed = console.nextInt();
		System.out.println();
		
		if(noOfPagesToBeFaxed <= 10)
			cost = SERVICE_CHARGES + BASIC_PAGE_FEE * noOfPagesToBeFaxed;
		else
			cost = SERVICE_CHARGES + BASIC_PAGE_FEE * 10 + 
				(noOfPagesToBeFaxed - 10) * ADDITIONAL_PAGE_FEE;
		
		System.out.printf("Cost = $%.2f%n", cost);
	}
}
