//**********************************************************************
// Author: Neil Kasanda
// 
// Program:
// Chapter 6, Exercise 8
// Modify the temperature conversion program to convert centimeters to
// inches, and vice versa.
//**********************************************************************

import javax.swing.*; // for JFrame and other GUI components
import java.awt.*;	// For Container class
import java.awt.event.*;	// For events

public class Exercise8_CentimeterToInchesConv extends JFrame 
{
	public JLabel centimeterL, inchL;
	
	public JTextField centimeterTF, inchTF;
	
	public CentimeterHandler centHandler;
	public InchHandler inchHandler;
	
	public final int WIDTH = 500;
	public final int HEIGHT = 100;
	
	public final double CENTIMETERS_PER_INCH = 2.54;
	
	public Exercise8_CentimeterToInchesConv()
	{
		setTitle("Centimeters to Inches (and vice versa) Conversion");
		
		centimeterL = new JLabel("Len in Centimeters: ");
		inchL = new JLabel("Len in Inches: ");
		
		centimeterTF = new JTextField(7);
		inchTF = new JTextField(7);
		
		centimeterTF.addActionListener(new CentimeterHandler());
		inchTF.addActionListener(new InchHandler());
		
		Container pane = getContentPane();
		pane.setLayout(new GridLayout(1, 4));
		
		pane.add(centimeterL);
		pane.add(centimeterTF);
		pane.add(inchL);
		pane.add(inchTF);
		
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	private class CentimeterHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			double centimeters, inches;
			
			centimeters = Double.parseDouble(centimeterTF.getText());
			
			inches = centimeters / CENTIMETERS_PER_INCH;
			
			inchTF.setText(String.format("%.2f", inches));
		}
	}
	
	private class InchHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			double centimeters, inches;
			
			inches = Double.parseDouble(inchTF.getText());
			
			centimeters = inches * CENTIMETERS_PER_INCH;
			
			centimeterTF.setText(String.format("%.2f", centimeters));
		}
	}
	
	public static void main(String[] args) 
	{
		Exercise8_CentimeterToInchesConv progObject = new Exercise8_CentimeterToInchesConv();
	}
}
