//***************************************************************************
// Author: Neil Kasanda
// 
// Program:
// Write a program that uses the method sqrt of the class Math and outputs
// the square roots of the first 25 positive integers. (Your program must output
// each number and its square root.)
//***************************************************************************

public class Chapter7_ProgProblem3 
{
	public static void main(String[] args) 
	{
		System.out.println("This program outputs the square roots of the"
				+ " first 25 positive integers.\n");
		for(int i = 1; i <= 25; i++)
		{
			System.out.println(i + ", sqrt(" + i + "): " + Math.sqrt(i));
		}
	}
}
