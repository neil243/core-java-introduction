//***************************************************
// Author: Neil Kasanda
//
// Program that produces a specified output.
//***************************************************
public class Chapter2_ProgProblem1 
{
	public static void main(String[] args) 
	{
		System.out.println("**********************************");
		System.out.println("*    Programming Assignment 1    *");
		System.out.println("*     Computer Programming I     *");
		System.out.println("*      Author: Duffy Ducky       *");
		System.out.println("*   Due Date: Thursday, Jan. 24  *");
		System.out.println("**********************************");
	}
}
