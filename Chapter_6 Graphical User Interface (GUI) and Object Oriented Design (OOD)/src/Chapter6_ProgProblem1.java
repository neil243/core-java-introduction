//*******************************************************************
// Author: Neil Kasanda
// 
// Program:
// Design a GUI program to find the weighted average of four test scores. The
// four test scores and their respective weights are given in the following
// format:
// testscore1 weight1
// ...
// For example, the sample data is as follows:
// 75 0.20
// 95 0.35
// 85 0.15
// 65 0.30
// The user is supposed to enter the data and press a Calculate button. The
// program must display the weighted average.
//*******************************************************************

import javax.swing.*;		// For JFrame and other GUIs
import java.awt.*;			// For Container class
import java.awt.event.*;	// For events

public class Chapter6_ProgProblem1 extends JFrame
{
	private JLabel score1L, score2L, score3L, score4L;
	private JLabel weight1L, weight2L, weight3L, weight4L;
	private JLabel averageL;
	
	private JTextField score1TF, score2TF, score3TF, score4TF;
	private JTextField weight1TF, weight2TF, weight3TF, weight4TF;
	private JTextField averageTF;
	
	private JButton calculateB, exitB;
	
	private final int WIDTH = 600;
	private final int HEIGHT = 500;
	
	private final int NUMBER_OF_TESTS = 4;
	
	public Chapter6_ProgProblem1()
	{
		setTitle("Weighted Average Calculator");
		
				// Instantiate all GUIs //
			// Labels
		score1L = new JLabel("Score: ", SwingConstants.RIGHT);
		weight1L = new JLabel("Weight: ", SwingConstants.RIGHT);
		score2L = new JLabel("Score: ", SwingConstants.RIGHT);
		weight2L = new JLabel("Weight: ", SwingConstants.RIGHT);
		score3L = new JLabel("Score: ", SwingConstants.RIGHT);
		weight3L = new JLabel("Weight: ", SwingConstants.RIGHT);
		score4L = new JLabel("Score: ", SwingConstants.RIGHT);
		weight4L = new JLabel("Weight: ", SwingConstants.RIGHT);
		averageL = new JLabel("Average: ", SwingConstants.RIGHT);
		
			// Text fields
		score1TF = new JTextField(5);
		weight1TF = new JTextField(5);
		score2TF = new JTextField(5);
		weight2TF = new JTextField(5);
		score3TF = new JTextField(5);
		weight3TF = new JTextField(5);
		score4TF = new JTextField(5);
		weight4TF = new JTextField(5);
		averageTF = new JTextField(5);
		
			// Buttons
		calculateB = new JButton("Calculate");
		exitB = new JButton("Exit");
		
			// Register needed listeners with respective GUIs
		calculateB.addActionListener(new CalculateButtonHandler());
		exitB.addActionListener(new ExitButtonHandler());
		
			// Get window pane
		Container pane = getContentPane();
		
			// Set container layout
		pane.setLayout(new GridLayout(5, 4));
		
			// add GUI elements to container pane 
		pane.add(score1L);
		pane.add(score1TF);
		pane.add(weight1L);
		pane.add(weight1TF);
		pane.add(score2L);
		pane.add(score2TF);
		pane.add(weight2L);
		pane.add(weight2TF);
		pane.add(score3L);
		pane.add(score3TF);
		pane.add(weight3L);
		pane.add(weight3TF);
		pane.add(score4L);
		pane.add(score4TF);
		pane.add(weight4L);
		pane.add(weight4TF);
		pane.add(calculateB);
		pane.add(exitB);
		pane.add(averageL);
		pane.add(averageTF);
		
			// Finish window setup
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	private class CalculateButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			double score1, score2, score3, score4, average;
			double weight1, weight2, weight3, weight4;
			
			score1 = Double.parseDouble(score1TF.getText());
			score2 = Double.parseDouble(score2TF.getText());
			score3 = Double.parseDouble(score3TF.getText());
			score4 = Double.parseDouble(score4TF.getText());
			
			weight1 = Double.parseDouble(weight1TF.getText());
			weight2 = Double.parseDouble(weight2TF.getText());
			weight3 = Double.parseDouble(weight3TF.getText());
			weight4 = Double.parseDouble(weight4TF.getText());
			
			average = (score1 * weight1) + (score2 * weight2) + 
					(score3 * weight3) + (score4 * weight4);
			
			averageTF.setText(String.format("%.2f", average));
		}
	}
	
	private class ExitButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			System.exit(0);
		}
	}
	
	public static void main(String[] args) 
	{
		Chapter6_ProgProblem1 progObj = new Chapter6_ProgProblem1(); 
	}
}
