//******************************************************************
// Author: Neil Kasanda
// 
// Program
// class Dispenser
//******************************************************************

public class Dispenser 
{
	private int numberOfItems;		// variable to store the number of
									// items in the dispenser
	private int cost;				// variable to store the cost of an item
	
	
		// Default constructor
		// Sets the number of items and the cost to 50 each
		// Postcondition: numberOfItems = 50; cost = 50;
	public Dispenser()
	{
		numberOfItems = 50;
		cost = 50;
	}
	
		// Constructor with parameters
		// Sets the values of numberOfItems and cost according
		// to the parameters.
		// Postcondition: numberOfItems = noOfItems; cost = costPerItem;
	public Dispenser(int noOfItems, int costPerItem)
	{
		if(noOfItems >= 0)
			numberOfItems = noOfItems;
		else
			numberOfItems = 50;
		
		if(costPerItem >= 0)
			cost = costPerItem;
		else
			cost = 50;
	}
	
		// Method to return the number of items in the dispenser
		// Postcondition: the value of numberOfItems is returned
	public int getCount()
	{
		return numberOfItems;
	}
	
		// Method to return the cost of item
		// Postcondition: the value of cost is returned.
	public int getProductCost()
	{
		return cost;
	}
	
	public void makeSale()
	{
		numberOfItems--;
	}
}
