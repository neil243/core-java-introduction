//***********************************************************************************
// Author: Neil Kasanda
//
// Program: Chapter 10, Programming Problem 5
// Every cylinder has a base and height, where the base is a circle.Design the class
// Cylinder that can capture the properties of a cylinder and perform the usual
// operations on a cylinder. Derive this class from the class Circle designed in
// Programming Exercise 4. Some of the operations that can be performed on a
// cylinder are as follows: calculate and print the volume, calculate and print the
// surface area, set the height, set the radius of the base, and set 
// the center of the base.
//************************************************************************************
public class Chapter10_ProgProblem5_Cylinder 
	extends Chapter10_ProgProblem4_Circle
{
	private double height;
	
	public Chapter10_ProgProblem5_Cylinder()
	{
		super();
		height = 0;
	}
	
	public Chapter10_ProgProblem5_Cylinder(double x, double y, double rad, double h)
	{
		super(x, y, rad);
		if(h >= 0)
			height = h;
		else
			height = 0;
	}
	
	public void setDimensions(double x, double y, double rad, double h)
	{
		super.setDimensions(x, y, rad);
		if(h >= 0)
			height = h;
		else
			height = 0;
	}
	
	public void setHeight(double h)
	{
		if(h >= 0)
			height = h;
		else
			height = 0;
	}
	
	public void setCenterOfBase(double x, double y)
	{
		super.setCoordinates(x, y);
	}
	
	public double area()
	{
		return 2 * Math.PI * getRadius() * height + 
				2 * Math.PI * getRadius() * getRadius();
	}
	
	public double volume()
	{
		return Math.PI * getRadius() * getRadius() * height;
	}
	
	public String toString()
	{
		return "Center of base = " + "(" + getXCoordinate() + ", " + 
					getYCoordinate() + ")" +
				"\nRadius of base = " + getRadius() + 
				"\nSurface Area = " + String.format("%.2f", area()) + 
				"\nVolume = " + String.format("%.2f", volume());
	}
}
