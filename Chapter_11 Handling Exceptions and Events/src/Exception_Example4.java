//**********************************************************************
// Author: Neil Kasanda
//
// Program:
// To illustrate handling of exception
//**********************************************************************

import javax.swing.JOptionPane;

public class Exception_Example4 
{
	public static void main(String[] args)
	{
		int dividend, divisor, quotient;
		String inpStr;
		
		try
		{
			inpStr = JOptionPane.showInputDialog("Enter the dividend: ");
			dividend = Integer.parseInt(inpStr);
			
			inpStr = JOptionPane.showInputDialog("Enter the divisor: ");
			divisor = Integer.parseInt(inpStr);
			
			quotient = dividend / divisor;
			
			JOptionPane.showMessageDialog(null, 
					"Line 10: \nDividend = " + dividend
					+ "\nDivisor = " + divisor
					+ "\nQuotient = " + quotient, 
					"Quotient", JOptionPane.INFORMATION_MESSAGE);
		}
		catch(ArithmeticException aeRef)
		{
			JOptionPane.showMessageDialog(null, "Line 12: Exception " + 
					aeRef.toString(), "ArithmeticException", 
					JOptionPane.ERROR_MESSAGE);
		}
		catch(NumberFormatException nfeRef)
		{
			JOptionPane.showMessageDialog(null, "Line 14: Exception " + 
					nfeRef.toString(), "NumberFormatException", 
					JOptionPane.ERROR_MESSAGE);
		}
		
		System.exit(0);
	}
}
