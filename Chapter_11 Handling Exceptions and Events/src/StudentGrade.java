//**********************************************************************
// Author: Neil Kasanda
//
// Program: Calculate the average test score
// This program shows how to handle a FileNotFoundException
// or any other exception.
//**********************************************************************
import java.util.*;
import java.io.*;

public class StudentGrade 
{
	public static void main(String[] args)
	{
			// Declare and initialize the variables
		double test1, test2, test3, test4, test5;
		double average;
		String firstName;
		String lastName;
		
		try
		{
			Scanner inFile = new Scanner(new FileReader("test.txt"));
			PrintWriter outFile = new PrintWriter("testavg.out");
			
			firstName = inFile.next();
			lastName = inFile.next();
			
			outFile.println("Student Name: " + firstName + " " + lastName);
			
				// Retrieve the five test scores
			test1 = inFile.nextDouble();
			test2 = inFile.nextDouble();
			test3 = inFile.nextDouble();
			test4 = inFile.nextDouble();
			test5 = inFile.nextDouble();
			
			outFile.printf("Test scores: %5.2f %5.2f %5.2f %5.2f %5.2f %n",
					test1, test2, test3, test4, test5);
			
			average = (test1 + test2 + test3 + test4 + test5) / 5.0;
			
			outFile.printf("Average test score: %5.2f %n", average);
			
			inFile.close();
			outFile.close();
		}
		catch(FileNotFoundException fnfeRef)
		{
			System.out.println(fnfeRef.toString());
		}
		catch(Exception eRef)
		{
			System.out.println(eRef.toString());
		}
	}
}