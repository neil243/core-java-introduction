//****************************************************************
// Author: Neil Kasanda
//
// Program: Chapter 10, Programming Problem 2
// In this chapter, the class Date was designed to implement the date in a
// program, but the method setDate and the constructor with parameters do
// not check whether the date is valid before storing the date in the data members.
// Rewrite the definitions of the method setDate and the constructor with
// parameters so that the values of month, day, and year are checked before storing
// the date into the datamembers. Add amethod isLeapYear to check whether a
// year is a leap year. Then, write a test program to test your class.
//****************************************************************

public class Chapter10_ProgProblem2_Date 
{
	private int dMonth;	// variable to store the month
	private int dDay;	// variable to store the day
	private int dYear;	// variable to store the year
	
		// Default constructor
		// The instance variables dMonth, dDay, and dYear are set
		// to the default values.
		// Postcondition: dMonth = 1; dDay = 1; dYear = 1900;
	public Chapter10_ProgProblem2_Date()
	{
		dMonth = 1;
		dDay = 1;
		dYear = 1900;
	}
	
		// Constructor to set the date
		// The instance variables dMonth, dDay, and dYear are set
		// according to the parameters.
		// Postcondition: dMonth = month; dDay = day;
		// 				  dYear = year;
	public Chapter10_ProgProblem2_Date(int month, int day, int year)
	{
		setDate(month, day, year);
	}
	
		// Method to set the date
		// The instance variables dMonth, dDay, and dYear are set
		// according to the parameters.
		// Postcondition: dMonth = month; dDay = day; dYear = year;
	public void setDate(int month, int day, int year)
	{
		if((1 <= month && month <= 12) 
				&& (1 <= day && day <= 31) 
				&& (year >= 1900) )
		{
			dMonth = month;
			dYear = year;
			
			dDay = 1;	// Handles invalid ranges of dDay
			
			switch(month)
			{
			case 2:
				if(isLeapYear(year))
				{
					if(1 <= day && day <= 29)
						dDay = day;
				}
				else if(1 <= day && day <= 28)	// Not a leap year
					dDay = day;
				break;
				
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				if(1 <= day && day <= 31)
					dDay = day;
				break;
				
			case 4:
			case 6:
			case 9:
			case 11:
				if(1 <= day && day <= 30)
					dDay = day;
				break;
			}
		}
		
		else
		{
			dMonth = 1;
			dDay = 1;
			dYear = 1900;
		}
	}
	
		// Method to return the month
		// Postcondition: The value of dMonth is returned
	public int getMonth()
	{
		return dMonth;
	}
	
		// Method to return the day
		// Postcondition: The value of dDay is returned
	public int getDay()
	{
		return dDay;
	}
	
		// Method to return the year
		// Postcondition: the value of dYear is returned
	public int getYear()
	{
		return dYear;
	}
	
	public boolean isLeapYear(int year)
	{
		if( (year % 4 == 0) || ( (year % 100 == 0) && (year % 400 == 0) ) )
			return true;
		
		return false;
	}
	
		// Method to return the date in the form mm-dd-yyyy
	public String toString()
	{
		return dMonth + "-" + dDay + "-" + dYear;
	}
	
	public static void main(String[] args)
	{
		Chapter10_ProgProblem2_Date date = 
				new Chapter10_ProgProblem2_Date();
		
		System.out.println("Line 3: date: " + date);
		System.out.println();
		
		date.setDate(6, 30, 1960);
		System.out.println("Line 6: After setting date: " + date);
		System.out.println();
		
		System.out.println("Line 8: Attempting to set an invalid date with "
				+ "invalid value ranges...");
		date.setDate(15, 54, 190);
		System.out.println("Line 10: date: " + date);
		System.out.println();
		
		System.out.println("Line 12: Valid leap year date...");
		date.setDate(2, 29, 2020);
		System.out.println("Line 14: date: " + date);
		System.out.println();
		
		System.out.println("Line 12: Invalid leap year date with valid value ranges...");
		date.setDate(2, 30, 2020);
		System.out.println("Line 14: date: " + date);
	}
}
