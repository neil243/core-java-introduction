//*******************************************************************
// Author: Neil Kasanda
// 
// Program: Largest Number
// This program determines the largest number of a set of
// 10 numbers.
//*******************************************************************

import java.util.*;

public class LargestNumber 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		double num;	// variable to hold the current number
		double max;	// variable to hold the larger number
		int count;	// loop control variable
		
		System.out.println("Enter 10 numbers: ");
		
		num = console.nextDouble();
		max = num;
		
		for(count = 1; count < 10; count++)
		{
			num = console.nextDouble();
			max = larger(max, num);
		}
		
		System.out.println("The largest number is " + max);
	}
	
	public static double larger(double x, double y)
	{
		double max;
		
		if(x >= y)
			max = x;
		else
			max = y;
		
		return max;
	}
}
