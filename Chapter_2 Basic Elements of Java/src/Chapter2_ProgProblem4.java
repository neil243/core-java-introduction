//************************************************
// Author: Neil Kasanda
//
// Program:
// a. Write a Java statement that imports the class Scanner.
// b. Write a Java statement that declares console to be a Scanner object for
// 		inputting data from the standard input device.
// c. Write Java statements that declare and initialize the following named
// 		constants: SECRET of type int initialized to 11; RATE of type double
// 		initialized to 12.50.
// d. Write Java statements that declare the following variables: num1, num2,
// 		and newNum of type int; name of type String; hoursWorked and
// 		wages of type double.
// e. Write Java statements that prompt the user to input two integers
// 		and store the first number into num1 and the second number into
// 		num2.
// f. Write a Java statement(s) that outputs the value of num1 and num2,
// 		indicating which is num1 and which is num2. For example, if num1 is 8
// 		and num2 is 5, then the output is:
// 		The value of num1 = 8 and the value of num2 = 5.
// g. Write a Java statement that multiplies that value of num1 by 2, adds the
// 		value of num2 to it, and then stores the result in newNum. Then write a
// 		Java statement that outputs the value of newNum.
// h. Write a Java statement that updates the value of newNum by adding the
// 		value of the named constant SECRET. Then, write a Java statement that
// 		outputs the value of newNum with an appropriate message.
// i. Write Java statements that prompt the user to enter a person�s last name
// 		and then store the last name into the variable name.
// j. Write Java statements that prompt the user to enter a decimal number
// 		between 0 and 70 and then store the number entered into hoursWorked.
// k. Write a Java statement that multiplies that value of the named constant
// 		RATE with the value of hoursWorked and stores the result into the
// 		variable wages.
// l. Write Java statements that produce the following output:
// 		Name: //output the value of the variable name
// 		Pay Rate: $ //output the value of the named constant RATE
// 		Hours Worked: //output the value of the variable hoursWorked
// 		Salary: $ //output the value of the variable wages
// 		For example, if the value of name is "Rainbow" and hoursWorked is
// 		45.50, then the output is:
// 		Name: Rainbow
// 		Pay Rate: $12.50
// 		Hours Worked: 45.50
// 		Salary: $568.75
// m. Write a Java program that tests each of the Java statements in parts (a)�
// 		(l). Place the statements at the appropriate place in the preceding Java
// 		program segment. Test run your program (twice) on the following input
// 		data:
// 		i. num1 = 13, num2 = 28; name = "Jacobson"; hoursWorked = 48.30.
// 		ii. num1 = 32, num2 = 15; name = "Cynthia"; hoursWorked = 58.45.
//************************************************

import java.util.Scanner;

public class Chapter2_ProgProblem4 
{
	static Scanner console = new Scanner(System.in);
	
	static final int SECRET = 11;
	static final double RATE = 12.50;
	
	public static void main(String[] args) 
	{
		int num1;
		int num2;
		int newNum;
		
		String name;
		
		double hoursWorked;
		double wages;
		
		System.out.print("Enter two integers separated by a space: ");
		num1 = console.nextInt();
		num2 = console.nextInt();
		System.out.println();
		
		System.out.println("The value of num1 = " + num1 +
				" and the value of num2 = " + num2);
		
		newNum = num1 * 2 + num2;
		System.out.println("The value of newNum = " + newNum);
		
		newNum = newNum + SECRET;
		System.out.println("The new value of newNum = " + newNum);
		
		System.out.print("Enter a last name: ");
		name = console.next();
		System.out.println();
		
		System.out.print("Enter a decimal number between 0 and 70: ");
		hoursWorked = console.nextDouble();
		System.out.println();
		
		wages = RATE * hoursWorked;
		
		System.out.println("Name: " + name);
		System.out.println("Pay Rate: $" + RATE);
		System.out.println("Hours Worked: " + hoursWorked);
		System.out.println("Salary: $" + wages);
	}
}
