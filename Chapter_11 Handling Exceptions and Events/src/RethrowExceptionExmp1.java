//**********************************************************************
// Author: Neil Kasanda
//
// Program: This program shows how to rethrow an exception.
//**********************************************************************

import java.util.*;

public class RethrowExceptionExmp1 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		int number;
		
		try
		{
			number = getNumber();
			System.out.println("Line 5: number = " + number);
		}
		catch(InputMismatchException imeRef)
		{
			System.out.println("Line 7: Exception " + imeRef.toString());
		}
		
		console.close();
	}
	
	public static int getNumber() throws InputMismatchException
	{
		int num;
		
		try
		{
			System.out.print("Line 11: Enter an integer: ");
			num = console.nextInt();
			System.out.println();
			
			return num;
		}
		catch(InputMismatchException imeRef)
		{
			throw imeRef;
		}
	}
}
