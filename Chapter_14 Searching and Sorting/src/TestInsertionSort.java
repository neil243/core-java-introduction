
public class TestInsertionSort 
{
	public static void main(String[] args)
	{
		int[] list = {2, 56, 34, 25, 73, 46, 89,
				  10, 5, 16};
	
		insertionSort(list, list.length);
	
		System.out.println("After sorting, the list elements are: ");
	
		for(int i = 0; i < list.length; i++)
			System.out.print(list[i] + " ");
	
		System.out.println();
	}
	
	public static void insertionSort(int[] list, int listLength)
	{
		int firstOutOfOrder, location;
		int temp;
		
		for(firstOutOfOrder = 1; firstOutOfOrder < listLength; firstOutOfOrder++)
		{
			if(list[firstOutOfOrder] < list[firstOutOfOrder - 1])
			{
				location = firstOutOfOrder;
				temp = list[firstOutOfOrder];
				
				do
				{
					list[location] = list[location - 1];
					location--;
				}
				while(location > 0 && list[location - 1] > temp);
				
				list[location] = temp;
			}
		}
	}
}
