//***********************************************************************
// Author: Neil Kasanda
// 
// Program:
// testing objects as variable length formal parameters
//***********************************************************************

public class Example_9_11_ObjectsAsVariableLengthParameters 
{
	public static void main(String[] args)
	{
		Clock myClock = new Clock(12, 5, 10);
		Clock yourClock = new Clock(8, 15, 6);
		
		Clock[] arrivalTimeEmp = new Clock[10];
		
		for(int j = 0; j < arrivalTimeEmp.length; j++)
			arrivalTimeEmp[j] = new Clock();
		
		arrivalTimeEmp[5].setTime(8, 5, 10);
		
		printTimes(myClock, yourClock);
		
		System.out.println("\n****************************\n");
		
		printTimes(arrivalTimeEmp);
		printTimes();
	}
	
	public static void printTimes(Clock ... clockList)
	{
		if(clockList.length != 0)
			for(int i = 0; i < clockList.length; i++)
				System.out.println(clockList[i]);
		else
			System.out.println("No clocks to print.");
	}
}
