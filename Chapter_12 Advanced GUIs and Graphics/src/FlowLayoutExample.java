//*****************************************************************
// Author: Neil Kasanda
// 
// Program:
// to illustrate FlowLayout.
//*****************************************************************

import javax.swing.*;
import java.awt.*;

public class FlowLayoutExample extends JFrame
{
	private final int WIDTH = 350;
	private final int HEIGHT = 350;
	
		// Variables to create GUI components
	private JLabel labelJL;
	private JTextField textFieldTF;
	private JButton buttonJB;
	private JCheckBox checkboxCB;
	private JRadioButton radioButtonRB;
	private JTextArea textAreaTA;
	
	private FlowLayout flowLayoutMgr;
	
	public FlowLayoutExample()
	{
		setTitle("FLowLayout Manager");
		setSize(WIDTH, HEIGHT);
		Container pane = getContentPane();
		
		flowLayoutMgr = new FlowLayout();
		pane.setLayout(flowLayoutMgr);
		
		labelJL = new JLabel("First Component");
		textFieldTF = new JTextField(15);
		textFieldTF.setText("Second Component");
		buttonJB = new JButton("Third Component");
		
		checkboxCB = new JCheckBox("Fourth Component");
		
		radioButtonRB = new JRadioButton("Fifth Component");
		
		textAreaTA = new JTextArea(10, 20);
		
		textAreaTA.setText("Sixth Component.\n");
		
		textAreaTA.append("Use the mouse to resize the window.");
		
			// place the GUI components into the pane
		pane.add(labelJL);
		pane.add(textFieldTF);
		pane.add(buttonJB);
		pane.add(checkboxCB);
		pane.add(radioButtonRB);
		pane.add(textAreaTA);
		
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args)
	{
		FlowLayoutExample flow = new FlowLayoutExample();
	}
}
