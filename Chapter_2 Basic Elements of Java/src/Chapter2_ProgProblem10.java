//*********************************************************
// Author: Neil Kasanda
//
// Problem:
// Write a program that prompts the capacity, in gallons, of an automobile fuel
// tank and the miles per gallons the automobile can be driven. The program
// outputs the number of miles the automobile can be driven without refueling.
//
//*********************************************************

import java.util.*;

public class Chapter2_ProgProblem10 
{
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		double capacity;
		double mpg;
		double miles;
		
		System.out.print("Enter the capacity, in gallons, of the automobile's fuel tank: ");
		capacity = console.nextDouble();
		
		System.out.print("Enter the fuel economy in miles per gallon (mpg): ");
		mpg = console.nextDouble();
		
		miles = capacity * mpg;
		System.out.println("With a full tank, and without being refueled, "
				+ "this automobile can travel " + miles + " mile(s).");
	}
}
