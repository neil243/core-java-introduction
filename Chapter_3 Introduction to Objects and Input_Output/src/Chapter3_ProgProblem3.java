//************************************************************
// Author: Neil Kasanda
// 
// Problem:
// Write a program that prompts the user to enter the weight of a person in
// kilograms and outputs the equivalent weight in pounds. Output both the
// weights rounded to two decimal places. (Note that 1 kilogram � 2.2
// pounds.) Format your output with two decimal places.
//************************************************************

import java.util.Scanner;

public class Chapter3_ProgProblem3 
{
	static Scanner console = new Scanner(System.in);
	
	static final double LBS_PER_KILO = 2.2;
	
	public static void main(String[] args) 
	{
		double weightKilos;
		double weightPounds;
		
		System.out.print("Enter a person's weight in kg and press enter: ");
		weightKilos = console.nextDouble();
		System.out.println();
		
		weightPounds = weightKilos * LBS_PER_KILO;
		System.out.printf("%.2f kg is equivalent to %.2f lbs", weightKilos, weightPounds);
	}
}
