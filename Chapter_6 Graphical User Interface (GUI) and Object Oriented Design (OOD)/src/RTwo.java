import javax.swing.*;
import java.awt.*;

public class RTwo extends JFrame
{
	private JLabel lengthL, widthL, areaL;
	
	private final int WIDTH = 400;
	private final int HEIGHT = 300;
	
	public RTwo()
	{
		setTitle("Good day Area");
		
		lengthL = new JLabel("Enter the length");
		widthL = new JLabel("Enter the width");
		areaL = new JLabel("Area: ");
		
		Container pane = getContentPane();
		pane.setLayout(new GridLayout(3,1));
		
		pane.add(lengthL);
		pane.add(widthL);
		pane.add(areaL);
		
		setSize(WIDTH,HEIGHT);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main(String args[])
	{
		RTwo R2 = new RTwo();
	}
}
