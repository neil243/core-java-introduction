//********************************************************************
// Author: Neil Kasanda
//
// Program:
// This program illustrates how to use a selection sort algorithm
// in a program.
//********************************************************************

public class TestSelectionSort 
{
	public static void main(String[] args)
	{
		int[] list = {2, 56, 34, 25, 73, 46, 89,
					  10, 5, 16};
		
		selectionSort(list, list.length);
		
		System.out.println("After sorting, the list elements are: ");
		
		for(int i = 0; i < list.length; i++)
			System.out.print(list[i] + " ");
		
		System.out.println();
	}
	
	public static void selectionSort(int[] list, int listLength)
	{
		int index;
		int smallestIndex;
		int minIndex;
		int temp;
		
		for(index = 0; index < listLength - 1; index++)
		{
			smallestIndex = index;
			
				// Step a: find smallest element in rest of list
			for(minIndex = index + 1; minIndex < listLength; minIndex++)
				if(list[minIndex] < list[smallestIndex])
					smallestIndex = minIndex;
			
				// Step b: move smallest element to front of unsorted list
			temp = list[smallestIndex];
			list[smallestIndex] = list[index];
			list[index] = temp;
		}
	}
}
