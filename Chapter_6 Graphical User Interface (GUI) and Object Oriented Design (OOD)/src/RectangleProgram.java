//********************************************************************
// Author: Neil Kasanda
// 
// Program:
// Given the length and width of a rectangle, this Java
// program determines its area and perimeter.
//********************************************************************

import java.awt.*; // for Container class
import java.awt.event.*; // for ActionListener class
import javax.swing.*;	// for GUI components

public class RectangleProgram extends JFrame 
{
		// Declare the labels
	private JLabel lengthL, widthL, areaL, perimeterL;
	
		// Declare the text fields
	private JTextField lengthTF, widthTF, areaTF, perimeterTF;
	
		// Declare the buttons
	private JButton calculateB, exitB;
	
		// Declare the action events handlers
	private CalculateButtonHandler cbHandler;
	private ExitButtonHandler ebHandler;
	
		// Declare the named constants for the
		// dimensions of the window
	private final int WIDTH = 400;
	private final int HEIGHT = 300;
	
	public RectangleProgram()
	{
		setTitle("Area and Perimeter of a Rectangle");
		
			// Create the four labels
		lengthL = new JLabel("Enter the length: ", SwingConstants.RIGHT);
		widthL = new JLabel("Enter the width: ", SwingConstants.RIGHT);
		areaL = new JLabel("Area: ", SwingConstants.RIGHT);
		perimeterL = new JLabel("Perimeter: ", SwingConstants.RIGHT);
		
			// Create the four text fields
		lengthTF = new JTextField(10);
		widthTF = new JTextField(10);
		areaTF = new JTextField(10);
		perimeterTF = new JTextField(10);
		
			// Create Calculate Button
		calculateB = new JButton("Calculate");
		cbHandler = new CalculateButtonHandler();
		calculateB.addActionListener(cbHandler);
		
			// Create Exit Button
		exitB = new JButton("Exit");
		ebHandler = new ExitButtonHandler();
		exitB.addActionListener(ebHandler);
		
			// Set the title of the window
		setTitle("Area and Perimeter of a Rectangle");
		
			// Get the container
		Container pane = getContentPane();
		
			// Set the layout
		pane.setLayout(new GridLayout(5, 2));
		
			// Place the components in the pane.
		pane.add(lengthL);
		pane.add(lengthTF);
		pane.add(widthL);
		pane.add(widthTF);
		pane.add(areaL);
		pane.add(areaTF);
		pane.add(perimeterL);
		pane.add(perimeterTF);
		pane.add(calculateB);
		pane.add(exitB);
		
			// Set the size of the window and display it
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	private class CalculateButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			double length, width, area, perimeter;
			
			length = Double.parseDouble(lengthTF.getText());
			width = Double.parseDouble(widthTF.getText());
			
			area = length * width;
			perimeter = 2 * (length + width);
			
			areaTF.setText("" + area);
			perimeterTF.setText("" + perimeter);
		}
	}
	
	private class ExitButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			System.exit(0);
		}
	}
	
	public static void main(String[] args)
	{
		RectangleProgram rectObject = new RectangleProgram();
	}
}
