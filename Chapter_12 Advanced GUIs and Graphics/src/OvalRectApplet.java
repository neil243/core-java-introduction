//*****************************************************************
// Author: Neil Kasanda
// 
// Program:
// OvalRectApplet Applet
//*****************************************************************

import java.awt.*;
import javax.swing.*;

public class OvalRectApplet extends JApplet
{
	private final int SIZE = 200;
	
	public void paint(Graphics g)
	{
		super.paint(g);
		
		int shape;
		int numOfFigures;
		int x;
		int y;
		int width;
		int height;
		int red;
		int green;
		int blue;
		
		int i;
		
			// determine the number of figures
		numOfFigures = 5 + (int)(Math.random() * 10);
		
		for(i = 0; i < numOfFigures; i++)
		{
			red = (int) (Math.random() * 256);	// red component
			green = (int) (Math.random() * 256);	// green component
			blue = (int) (Math.random() * 256);	// blue component
			
			g.setColor(new Color(red, green, blue));
			
			x = (int) (Math.random() * SIZE);	// x value of anchor point
			y = (int) (Math.random() * SIZE);	// y value of anchor point
			width = (int) (Math.random() * SIZE);	// width
			height = (int) (Math.random() * SIZE);	// height
			
			shape = (int)(Math.random() * 4);
			/**
			 * 		0 : Rectangle
			 * 		1 : Filled Rectangle
			 * 		2 : Oval
			 * 		3 : Filled Oval
			 * 
			**/
			
			switch(shape)
			{
			case 0:
				g.drawRect(x, y, width, height);
				break;
				
			case 1:
				g.fillRect(x, y, width, height);
				break;
				
			case 2:
				g.drawOval(x, y, width, height);
				break;
				
			case 3:
				g.fillOval(x, y, width, height);
				break;
			
			}// end switch
		}// end for
	}
}
