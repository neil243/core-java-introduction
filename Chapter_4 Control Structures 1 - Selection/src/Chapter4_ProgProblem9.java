//******************************************************************
// Author: Neil Kasanda
// 
// Program:
// Write a program that mimics a calculator. The program should take as input
// two integers and an arithmetic operation (+, -, *, or /) to be performed. It
// should then output the numbers, the operator, and the result. (For division,
// if the denominator is zero, output an appropriate message.) Some sample
// outputs follow:
// 3 + 4 = 7
// 13 * 5 = 65
//******************************************************************
import javax.swing.*;

public class Chapter4_ProgProblem9 
{
	public static void main(String[] args) 
	{
		int num1;
		int num2;
		String inputStr;
		String outputStr;
		
		inputStr = JOptionPane.showInputDialog("Enter the first integer");
		num1 = Integer.parseInt(inputStr);
		
		inputStr = JOptionPane.showInputDialog("Enter the second integer");
		num2 = Integer.parseInt(inputStr);
		
		inputStr = JOptionPane.showInputDialog("Enter the operation to perform");
		
		switch(inputStr.charAt(0))
		{
		case '+':
			outputStr = num1 + " + " + num2 + " = " + (num1 + num2);
			JOptionPane.showMessageDialog(null, outputStr, 
					"Simple Calculator", JOptionPane.INFORMATION_MESSAGE);
			break;
		
		case '-':
			outputStr = num1 + " - " + num2 + " = " + (num1 - num2);
			JOptionPane.showMessageDialog(null, outputStr, 
					"Simple Calculator", JOptionPane.INFORMATION_MESSAGE);
			break;
		
		case '/':
			if(num2 == 0)
			{
				JOptionPane.showMessageDialog(null, "Denominator must be nonzero", 
						"Simple Calculator", JOptionPane.INFORMATION_MESSAGE);
				break;
			}
			
			outputStr = num1 + " / " + num2 + " = " + (num1 / num2);
			JOptionPane.showMessageDialog(null, outputStr, 
					"Simple Calculator", JOptionPane.INFORMATION_MESSAGE);
			break;
		
		case '*':
			outputStr = num1 + " * " + num2 + " = " + (num1 * num2);
			JOptionPane.showMessageDialog(null, outputStr, 
					"Simple Calculator", JOptionPane.INFORMATION_MESSAGE);
			break;
		
		default:
			System.out.print("operation \"" + inputStr + "\" not supported");
		}
		
	}
}
