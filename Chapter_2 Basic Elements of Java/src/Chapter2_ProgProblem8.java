//*******************************************************
// Author: Neil Kasanda
//
// Write a program that prompts the user to input five decimal numbers. The
// program should then add the five decimal numbers, convert the sum to the
// nearest integer, and print the result.
//*******************************************************

import java.util.*;

public class Chapter2_ProgProblem8 
{
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		double num1, num2, num3, num4, num5, sum;
		int roundedSum;
		
		System.out.print("Enter 5 decimal numbers separated by spaces: ");
		num1 = console.nextDouble();
		num2 = console.nextDouble();
		num3 = console.nextDouble();
		num4 = console.nextDouble();
		num5 = console.nextDouble();
		System.out.println("The numbers you entered are: " + num1 + 
				" " + num2 + " " + num3 + " " + num4 + " " + num5);
		
		sum = num1 + num2 + num3 + num4 + num5;
		System.out.println("The sum of the five numbers = " + sum);
		
		roundedSum = (int) (sum + 0.5);
		System.out.println("The sum rounded to nearest integer = " + roundedSum);
	}
}
