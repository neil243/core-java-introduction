//******************************************************************
// Author: Neil Kasanda
//
// Program to test various operations of the class RollDie.
//******************************************************************

public class TestProgRollDie 
{
	public static void main(String[] args) 
	{
		RollDie die1 = new RollDie();
		RollDie die2 = new RollDie();
		
		System.out.println("Line 9: die1: " + die1);
		System.out.println("Line 10: die2: " + die2);
		
		System.out.println("Line 11: After rolling die1: " + die1.roll());
		System.out.println("Line 12: After rolling die2: " + die2.roll());
		
		System.out.println("Line 13: Sum of the numbers rolled by "
				+ "the dice is: " + (die1.getNum() + die2.getNum()));
		
		System.out.println("Line 14: After rolling again, the sum "
				+ "of the numbers rolled is: " + (die1.roll() + die2.roll()));
	}// end main
}
