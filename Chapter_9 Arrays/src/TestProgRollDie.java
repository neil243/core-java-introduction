//*******************************************************************
// Author: Neil Kasanda
//
// Program:
// This program rolls 100 dice. It outputs the number of times
// each number is rolled, the number(s) that are rolled the
// maximum number of times, and the maximum roll count.
//*******************************************************************

import java.util.*;

public class TestProgRollDie 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		RollDie[] dice = new RollDie[100];
		
		int[] rollCount = new int[6];
		int maxNumRoll = 0;
		
		for(int i = 0; i < 100; i++)
		{
			dice[i] = new RollDie();
			dice[i].roll();
		}
		
		System.out.println("Numbers rolled: ");
		for(int i = 0; i < 100; i++)
		{
			int num = dice[i].getNum();
			
			System.out.print(" " + num);
			rollCount[num - 1]++;
			if((i + 1) % 34 == 0)
				System.out.println();
		}
		
		System.out.println();
		System.out.println("Num  Roll_Count");
		
		for(int i = 0; i < 6; i++)
		{
			System.out.println(" " + (i + 1) + "        " + rollCount[i]);
			
			if(rollCount[i] > rollCount[maxNumRoll])
				maxNumRoll = i;
		}
		
		System.out.print("The number(s) ");
		for(int i = 0; i < 6; i++)
		{
			if(rollCount[i] == rollCount[maxNumRoll])
			System.out.print((i + 1) + " ");
		}
		System.out.println("is (are) rolled the maximum number of times, which is "
				+ rollCount[maxNumRoll] + ".");
		
		console.close();
	}//end main
}
