//*************************************************************
// Author: Neil Kasanda
// 
// Program: Find the sum of the numbers in a file in each line
// Data in file Exp_5_21.txt:
// 65 78 65 89 25 98 -999
// 87 34 89 99 26 78 64 34 -999
// 23 99 98 97 26 78 100 63 87 23 -999
// 62 35 78 99 12 93 19 -999
//*************************************************************

import java.util.*;
import java.io.*;

public class Example5_21 
{
	public static void main(String[] args) throws FileNotFoundException
	{
		Scanner inFile = new Scanner(new FileReader("Exp_5_21.txt"));
		
		int i;
		int sum;
		int num;
		
		for(i = 1; i <= 4; i++)
		{
			sum = 0;
			
			num = inFile.nextInt();
			
			while(num != -999)
			{
				sum = sum + num;
				num = inFile.nextInt();
			}
			
			System.out.printf("Line: %d: sum = %d%n", i, sum);
		}
		
		inFile.close();
	}
}
