import javax.swing.JOptionPane;

public class Example5_5_Telephone_Digits 
{
	static final char SENTINEL = '#';
	
	public static void main(String[] args) 
	{
		char letter;
		
		String inputMessage;
		String inputString;
		String outputMessage;
		
		inputMessage = "Program to convert uppercase " + 
						"Letters to their corresponding " +
						"telephone digits.\n" + 
						"To stop the program enter #.\n" + 
						"Enter a letter: ";
		
		inputString = JOptionPane.showInputDialog(inputMessage);
		
		letter = inputString.charAt(0); 
		
		while(letter != SENTINEL)
		{
			outputMessage = "The letter you entered is: " +
							letter + "\n" + 
							"The corresponding telephone "
							+ "digit is: ";
			
			if(letter >= 'A' && letter <= 'Z')
			{
				switch(letter)
				{
				case 'A':
				case 'B':
				case 'C':
					outputMessage = outputMessage + "2";
					break;
					
				case 'D':
				case 'E':
				case 'F':
					outputMessage = outputMessage + "3";
					break;
					
				case 'G':
				case 'H':
				case 'I':
					outputMessage = outputMessage + "4";
					break;
					
				case 'J':
				case 'K':
				case 'L':
					outputMessage = outputMessage + "5";
					break;
					
				case 'M':
				case 'N':
				case 'O':
					outputMessage = outputMessage + "6";
					break;
					
				case 'P':
				case 'Q':
				case 'R':
				case 'S':
					outputMessage = outputMessage + "7";
					break;
					
				case 'T':
				case 'U':
				case 'V':
					outputMessage = outputMessage + "8";
					break;
					
				case 'W':
				case 'X':
				case 'Y':
				case 'Z':
					outputMessage = outputMessage + "9";
					break;
				}
			}
			
			else
			{
				outputMessage = outputMessage + "Invalid Input";
			}
			
			JOptionPane.showMessageDialog(null, outputMessage, 
					"Telephone Digit", JOptionPane.PLAIN_MESSAGE);
			
			inputMessage = "Enter another uppercase letter " +
							"to find its corresponding " + 
							"telephone digit.\n" + 
							"To stop the program enter #.\n" + 
							"Enter a letter: ";
			
			inputString = JOptionPane.showInputDialog(inputMessage);
			
			letter = inputString.charAt(0);
		}//end while
		
		System.exit(0);
	}
}
