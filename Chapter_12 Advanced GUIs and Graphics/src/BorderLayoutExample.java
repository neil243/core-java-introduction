//*****************************************************************
// Author: Neil Kasanda
// 
// Program:
// to illustrate BorderLayout layout manager.
//*****************************************************************

import javax.swing.*;
import java.awt.*;

public class BorderLayoutExample extends JFrame
{
	private final int WIDTH = 350;
	private final int HEIGHT = 300;
	
		// GUI components
	private JLabel labelJL;
	private JTextField textFieldTF;
	private JButton buttonJB;
	private JCheckBox checkboxCB;
	private JRadioButton radioButtonRB;
	private JTextArea textAreaTA;
	
	private BorderLayout borderLayoutMgr;
	
	public BorderLayoutExample()
	{
		setTitle("BorderLayout Manager");
		setSize(WIDTH, HEIGHT);
		Container pane = getContentPane();
		
		borderLayoutMgr = new BorderLayout(10, 10);
		pane.setLayout(borderLayoutMgr);
		
		labelJL = new JLabel("North Component");
		textAreaTA = new JTextArea(10, 20);
		textAreaTA.setText("South Component.\n");
		textAreaTA.append("Use the mouse to change the size of the window.");
		
		buttonJB = new JButton("West Component");
		checkboxCB = new JCheckBox("East Component");
		radioButtonRB = new JRadioButton("Center Component");
		
		pane.add(labelJL, BorderLayout.NORTH);
		pane.add(textAreaTA, BorderLayout.SOUTH);
		pane.add(checkboxCB, BorderLayout.EAST);
		pane.add(buttonJB, BorderLayout.WEST);
		pane.add(radioButtonRB, BorderLayout.CENTER);
		
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args)
	{
		BorderLayoutExample border = new BorderLayoutExample();
	}
}
