//********************************************************************
// Author: Neil Kasanda
//
// The following program determines an employee's weekly wages. If the
// hours worked exceed 40, then wages include overtime payment.
//********************************************************************

import java.util.*;

public class Example4_12 
{
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		double wages, rate, hours;
		
		System.out.print("Line 2: Enter the working hours: ");
		hours = console.nextDouble();
		System.out.println();
		
		System.out.print("Line 5: Enter the pay rate: ");
		rate = console.nextDouble();
		System.out.println();
		
		if(hours > 40)
			wages = rate * 40 + 1.5 * rate * (hours - 40);
		else
			wages = rate * hours;
		
		System.out.printf("Line 12: The wages are $%.2f %n", wages);
		System.out.println();
	}
}
