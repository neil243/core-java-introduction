//**************************************************************************
// Author: Neil Kasanda
// 
// Program: Comparison of Class Averages
// This program computes and compares the class averages of
// two groups of students.
//**************************************************************************

import java.io.*;
import java.util.*;

public class DataComparison 
{
	public static void main(String[] args) throws FileNotFoundException
	{
			// Step 1
		String courseId1;	// course ID for group 1
		String courseId2;	// course ID for group 2
		
		int numberOfCourses;
		
		double avg1;	// average for a course in group 1
		double avg2;	// average for a course in group 2
		double avgGroup1;	// average group 1
		double avgGroup2;	// average group 2
		
			// Step 2 open the input and output files
		Scanner group1 = new Scanner(new FileReader("group1.txt"));
		Scanner group2 = new Scanner(new FileReader("group2.txt"));
		
		PrintWriter outFile = new PrintWriter("student.out");
		PrintWriter outFile2 = new PrintWriter("student2.out");
		
		avgGroup1 = 0.0;
		avgGroup2 = 0.0;
		
		numberOfCourses = 0;
		
			// Print heading
		outFile.println("Course ID	Group No	Course Average");
		printHeading(outFile2); // For bar graph file
		
		while(group1.hasNext() && group2.hasNext())
		{
			courseId1 = group1.next();
			courseId2 = group2.next();
			
			if(!courseId1.equals(courseId2))
			{
				System.out.println("Data error: Course IDs do not match.");
				System.out.println("Program terminates.");
				outFile.println("Data error: Course IDs do not match.");
				outFile.println("Program terminates.");
				outFile.close();
				return;
			}
			else
			{
				avg1 = calculateAverage(group1);
				avg2 = calculateAverage(group2);
				printResult(outFile, courseId1, 1, avg1);
				printResult(outFile, courseId2, 2, avg2);
				
				printResult2(outFile2, courseId1, 1, avg1);
				printResult2(outFile2, courseId2, 2, avg2);
				outFile.println();
				
				avgGroup1 = avgGroup1 + avg1;
				avgGroup2 = avgGroup2 + avg2;
				numberOfCourses++;
			}
		}// end while
		
		if(group1.hasNext() && !group2.hasNext()) 
		{
			System.out.println("Ran out of data for group 2 before group 1.");
		}
		else if(!group1.hasNext() && group2.hasNext())
		{
			System.out.println("Ran out of data for group 1 before group 2.");
		}
		else
		{
			outFile.printf("Avg for group 1: %.2f %n", avgGroup1 / numberOfCourses);
			outFile.printf("Avg for group 2: %.2f %n", avgGroup2 / numberOfCourses);
		}
		
		group1.close();
		group2.close();
		outFile.close();
		outFile2.close();
	}
	
	public static double calculateAverage(Scanner inp)
	{
		double totalScore = 0.0;
		int numberOfStudents = 0;
		int score = 0;
		double courseAvg;
		
		score = inp.nextInt();
		while(score != -999)
		{
			totalScore = totalScore + score;
			numberOfStudents++;
			score = inp.nextInt();
		}//end while
		
		courseAvg = totalScore / numberOfStudents;
		
		return courseAvg;
	}
	
	public static void printResult(PrintWriter outp, 
								String courseId, int groupNo, 
								double avg)
	{
		if(groupNo == 1)
			outp.print("  " + courseId + "  ");
		else
			outp.print("       ");
		outp.printf("%9d %15.2f%n", groupNo, avg);
	}
	
	public static void printResult2(PrintWriter outp, 
								String courseId, int groupNo, 
								double avg)
	{
		int noOfSymbols;
		int count;
		
		if(groupNo == 1)
			outp.print(" " + courseId + "     ");
		else
			outp.print("         ");
		
		noOfSymbols = (int) (avg) / 2;
		
		if(groupNo == 1)
			for(count = 1; count <= noOfSymbols; count++)
				outp.print("*");
		else
			for(count = 1; count <= noOfSymbols; count++)
				outp.print("#");
		
		outp.println();
	}// end printResult2
	
	public static void printHeading(PrintWriter outp)
	{
		outp.println("Course		Course Average");
		outp.println("  ID     0   10   20   30   40   50   60   70   80   90  100");
		outp.println("         |....|....|....|....|....|....|....|....|....|....|");
	}// end printHeading
}
