import javax.swing.JApplet;
import java.awt.Graphics;

public class WelcomeApplet extends JApplet 
{
	public void paint(Graphics g)
	{
		super.paint(g);
		g.drawString("Welcome to Java Programming", 30, 30);
	}
}
