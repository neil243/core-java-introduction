//***************************************************************************************
// Author: Neil Kasanda
//
// Program: Chapter 10, Programming Problem 4
// Every circle has a center and a radius. Given the radius, we can determine the
// circle�s area and circumference.Given the center, we can determine its position in
// the x-y plane. The center of a circle is a point in the x-y plane. Design the class
// Circle that can store the radius and center of the circle. Because the center is a
// point in the x-y plane and you designed the class to capture the properties of a
// point in Programming Exercise 3, you must derive the class Circle from the
// class Point. You should be able to perform the usual operations on a circle,
// such as setting the radius, printing the radius, calculating and printing the area and
// circumference, and carrying out the usual operations on the center.
//****************************************************************************************

public class Chapter10_ProgProblem4_Circle extends
	Chapter10_ProgProblem3_Point
{
	private double radius;
	
	public Chapter10_ProgProblem4_Circle()
	{
		super();
		radius = 0;
	}
	
	public Chapter10_ProgProblem4_Circle(double x, double y, double rad)
	{
		super(x, y);
		if(rad >= 0)
			radius = rad;
		else
			radius = 0;
	}
	
	public void setDimensions(double x, double y, double rad)
	{
		super.setCoordinates(x, y);
		if(rad >= 0)
			radius = rad;
		else
			radius = 0;
	}
	
	public void setRadius(double rad)
	{
		if(rad >= 0)
			radius = rad;
		else
			radius = 0;
	}
	
	public double getRadius()
	{
		return radius;
	}
	
	public double area()
	{
		return Math.PI * radius * radius;
	}
	
	public double circumference()
	{
		return 2 * Math.PI * radius;
	}
	
	public String toString()
	{
		return "center: " + super.toString() + "\n" +
				"radius = " + radius + "\n" +
				"Area = " + String.format("%.2f", area()) + "\n" +
				"Circumference = " + String.format("%.2f", circumference());
	}
	
}
