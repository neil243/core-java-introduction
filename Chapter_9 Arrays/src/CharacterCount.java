//**************************************************************
// Author: Neil Kasanda
//
// Program: Line and letter count
// This program reads a given text, outputs the text as
// is, and prints the number of lines and the number of times
// each letter appears in the text. An uppercase letter and a
// lowercase letter are treated as being the same; that is,
// they are tallied together.
//**************************************************************

import java.io.*;

public class CharacterCount 
{
	public static void main(String[] args) 
			throws FileNotFoundException, IOException 
	{
		int lineCount = 0;
		int[] letterCount = new int[26];
		
		int next;
		
		FileReader inputStream = new FileReader("text.txt");
		PrintWriter outFile = new PrintWriter("textCh.out");
		
		next = inputStream.read();
		
		while(next != -1)
		{
			next = copyText(inputStream, outFile, next, letterCount);
			
			lineCount++;
			next = inputStream.read();
		}//end while loop
		
		writeTotal(outFile, lineCount, letterCount);
		outFile.close();
	}
	
	public static int copyText(FileReader infile, 
			PrintWriter outfile, int next, int[] letterC) throws IOException
	{
		while(next != '\n' && next != -1)
		{
			outfile.print( (char) next );
			chCount((char) next, letterC);
			
			next = infile.read();
		}
		
		outfile.println();
		
		return next;
	}
	
	public static void chCount(char ch, int[] letterC)
	{
		int index;
		
		ch = Character.toUpperCase(ch);
		index = (int) ch - 65;
		
		if(0 <= index && index < 26)
			letterC[index]++;
	}
	
	public static void writeTotal(PrintWriter outfile, int lines, int[] letterC)
	{
		outfile.println();
		outfile.println("The number of lines = " + lines);
		
		for(int i = 0; i < letterC.length; i++)
			outfile.println((char)(i + 65) + " count " + letterC[i]);
	}
}
