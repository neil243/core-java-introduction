//******************************************************
// Author: Neil Kasanda
//
// This program originally had the following sematic error:
// celcius = 5 / 9 * (fahrenheit - 32) which would always
// equal 0 in Java because of the integer division 5 / 9,
// which yields 0.
//******************************************************

import java.util.Scanner;

public class LogicErrorCorrection 
{
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		int fahrenheit;
		int celsius;
		
		System.out.print("Enter temperature in Fahrenheit: ");
		fahrenheit = console.nextInt();
		System.out.println();
		
		celsius = (int) (5.0 / 9 * (fahrenheit - 32) + 0.5);
		System.out.println(fahrenheit + " degree F = " + celsius + " degree C.");
	}
}
