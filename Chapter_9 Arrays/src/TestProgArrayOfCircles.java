//**************************************************************************
// Author: Neil Kasanda
//
// Program:
// to create an array of circles
//**************************************************************************

import java.util.*;

public class TestProgArrayOfCircles 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		Circle[] circles = new Circle[5];
		
		double radius;
		
		for(int i = 0; i < 5; i++)
		{
			System.out.print("Enter the radius of circle " + (i + 1) + ": ");
			radius = console.nextDouble();
			circles[i] = new Circle(radius);
			System.out.println();
		}
		
		for(int i = 0; i < 5; i++)
			System.out.println("Circle " + (i + 1) + ": " + circles[i]);
		
		console.close();
	}//end main
}
