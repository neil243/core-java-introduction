//****************************************************************
// Author: Neil Kasanda
// 
// Program:
// class CashRegister
//****************************************************************

public class CashRegister 
{
	private int cashOnHand;
	
		// Default constructor to set the cash
		// in the register to 500 cents
		// Postcondition: cashOnHand = 500;
	public CashRegister()
	{
		cashOnHand = 500;
	}
	
		// Constructor with parameters to set the cash in
		// the register to the amount specified by cashIn
		// Postcondition: cashOnHand = cashIn;
	public CashRegister(int cashIn)
	{
		if(cashIn >= 0)
			cashOnHand = cashIn;
		else
			cashOnHand = 0;
	}
	
		// Method to show the current amount in the cash register
		// Postcondition: The value of the instance variable
		//					cashOnHand is returned.
	public int currentBalance()
	{
		return cashOnHand;
	}
	
		// Method to receive the amount deposited by the customer
		// The amount in the cash register is updated by adding
		// the deposited amount to the current cash on hand.
		// Postcondition: cashOnHand = cashOnHand + amountIn;
	public void acceptAmount(int amountIn)
	{
		cashOnHand = cashOnHand + amountIn;
	}
}
