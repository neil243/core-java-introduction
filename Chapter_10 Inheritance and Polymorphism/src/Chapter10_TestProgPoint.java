//*****************************************************************
// Author: Neil Kasanda
//
// Program:
// Program to test the various operations of a point.
//*****************************************************************
public class Chapter10_TestProgPoint 
{
	public static void main(String[] args) 
	{
		Chapter10_ProgProblem3_Point myPoint = 
				new Chapter10_ProgProblem3_Point();
		
		System.out.println("Line 7: myPoint: " + myPoint);
		System.out.printf("Line 9: xCoordinate = %.2f, yCoordinate = %.2f%n", 
				myPoint.getXCoordinate(), myPoint.getYCoordinate());
		System.out.println();
		
		myPoint.setCoordinates(5, 8);
		System.out.println("Line 13: After setting coordinates, myPoint:" + myPoint);
		System.out.printf("Line 15: xCoordinate = %.2f, yCoordinate = %.2f%n", 
				myPoint.getXCoordinate(), myPoint.getYCoordinate());
		System.out.println();
	}
}
