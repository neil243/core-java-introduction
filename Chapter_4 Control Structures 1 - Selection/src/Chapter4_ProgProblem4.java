//*****************************************************************
// Author: Neil Kasanda
//
// Program:
// The statements in the following program are in incorrect order. Rearrange the
// statements so that it prompts the user to input the shape type (rectangle,
// circle, or cylinder), and the appropriate dimension of the shape. The
// program then outputs the following information about the shape: For a
// rectangle, it outputs the area and perimeter; for a circle, it outputs the
// area and circumference; and for a cylinder, it outputs the volume and surface
// area. After rearranging the statements, your program should be properly
// indented.
//*****************************************************************

import java.util.*;

public class Chapter4_ProgProblem4 
{
	static Scanner console = new Scanner(System.in);
	
	static final double PI = 3.1416;
	
	public static void main(String[] args)
	{
		String shape;
		
		System.out.print("Enter the shape type: (rectangle, circle, cylinder): ");
		shape = console.next();
		System.out.println();
	
		if (shape.compareTo("rectangle") == 0)
		{
			double length;
			double width;
			
			System.out.print("Enter the length of the rectangle: ");
			length = console.nextDouble();;
			System.out.println();
			
			System.out.print("Enter the width of the rectangle: ");
			width = console.nextDouble();
			System.out.println();
			
			System.out.printf("Area of the rectangle = %.2f%n", (length * width));
			
			System.out.printf("Perimeter of the rectangle = %.2f%n", (2 * (length + width)));
		}
	
		else if (shape.compareTo("circle") == 0)
		{
			double radius;
			
			System.out.print("Enter the radius of the circle: ");
			radius = console.nextDouble();
			System.out.println();
			
			System.out.printf("Area of the circle = %.2f%n", (PI * Math.pow(radius, 2.0)));
			System.out.printf("Circumference of the circle: %.2f%n", (2 * PI * radius));
		}
	
		else if (shape.compareTo("cylinder") == 0)
		{
			double height;
			double radius;
			
			System.out.print("Enter the height of the cylinder: ");
			height = console.nextDouble();
			System.out.println();
			
			System.out.print("Enter the radius of the base of the cylinder: ");
			radius = console.nextDouble();
			System.out.println();
			
			System.out.printf("Volume of the cylinder = %.2f%n", (PI * Math.pow(radius, 2.0)* height));
			
			System.out.printf("Surface area of the cylinder: %.2f%n", 
					(2 * PI * radius * height + 2 * PI * Math.pow(radius, 2.0)));
		}
	
		else
			System.out.println("The program does not handle " + shape);
	}
}
