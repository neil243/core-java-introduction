//*****************************************************************
// Author: Neil Kasanda
// 
// Program: Chapter 12, Programming Problem 2
// Modify the applet created in Programming Exercise 1 by adding 
// eight radio buttons so the user can change the color of the 
// digit drawn. 
//*****************************************************************
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Chapter12_ProgProblem2 extends JApplet
	implements KeyListener, ItemListener
{
	private char input;
	private Color currentColor;
	
	private JRadioButton blackRB;
	private JRadioButton redRB;
	private JRadioButton greenRB;
	private JRadioButton blueRB;
	private JRadioButton cyanRB;
	private JRadioButton magentaRB;
	private JRadioButton yellowRB;
	private JRadioButton pinkRB;
	
	private ButtonGroup colorSelectGroup;
	
	public void init()
	{
		input = '*';
		currentColor = Color.black;
		
		blackRB = new JRadioButton("Black");
		redRB = new JRadioButton("Red");
		greenRB = new JRadioButton("Green");
		blueRB = new JRadioButton("Blue");
		cyanRB = new JRadioButton("Cyan");
		magentaRB = new JRadioButton("Magenta");
		yellowRB = new JRadioButton("Yellow");
		pinkRB = new JRadioButton("Pink");
		
		blackRB.setSize(100, 30);
		redRB.setSize(100, 30);
		greenRB.setSize(100, 30);
		blueRB.setSize(100, 30);
		cyanRB.setSize(100, 30);
		magentaRB.setSize(100, 30);
		yellowRB.setSize(100, 30);
		pinkRB.setSize(100, 30);
		
		blackRB.setLocation(0, 210);
		redRB.setLocation(0, 240);
		greenRB.setLocation(0, 270);
		blueRB.setLocation(0, 300);
		cyanRB.setLocation(150, 210);
		magentaRB.setLocation(150, 240);
		yellowRB.setLocation(150, 270);
		pinkRB.setLocation(150, 300);
		
		blackRB.addItemListener(this);
		redRB.addItemListener(this);
		greenRB.addItemListener(this);
		blueRB.addItemListener(this);
		cyanRB.addItemListener(this);
		magentaRB.addItemListener(this);
		yellowRB.addItemListener(this);
		pinkRB.addItemListener(this);
		
		colorSelectGroup = new ButtonGroup();
		colorSelectGroup.add(blackRB);
		colorSelectGroup.add(redRB);
		colorSelectGroup.add(greenRB);
		colorSelectGroup.add(blueRB);
		colorSelectGroup.add(cyanRB);
		colorSelectGroup.add(magentaRB);
		colorSelectGroup.add(yellowRB);
		colorSelectGroup.add(pinkRB);
		
		Container pane = getContentPane();
		pane.setLayout(null);
		pane.add(blackRB);
		pane.add(redRB);
		pane.add(greenRB);
		pane.add(blueRB);
		pane.add(cyanRB);
		pane.add(magentaRB);
		pane.add(yellowRB);
		pane.add(pinkRB);
		
		addKeyListener(this);
		setFocusable(true);	// to allow the window to generate key events
	}
	
	public void paint(Graphics g)
	{
		super.paint(g);
		
		g.setColor(currentColor);
		drawDigit(g);
	}
	
	public void keyTyped(KeyEvent event)
	{
		input = event.getKeyChar();
		repaint();
	}
	
	public void keyPressed(KeyEvent event) {}	
	public void keyReleased(KeyEvent event) {}
	
	public void itemStateChanged(ItemEvent event)
	{		
		if(event.getSource() == blackRB)
			currentColor = Color.black;
		
		else if(event.getSource() == redRB)
			currentColor = Color.red;
		
		else if(event.getSource() == greenRB)
			currentColor = Color.green;
		
		else if(event.getSource() == blueRB)
			currentColor = Color.blue;
		
		else if(event.getSource() == cyanRB)
			currentColor = Color.cyan;
		
		else if(event.getSource() == magentaRB)
			currentColor = Color.magenta;
		
		else if(event.getSource() == yellowRB)
			currentColor = Color.yellow;
		
		else if(event.getSource() == pinkRB)
			currentColor = Color.pink;
		
		repaint();
	}
	
	public void drawDigit(Graphics g)
	{		
		switch(input)
		{
		case '0':
			draw0(g);
			break;
		case '1':
			draw1(g);
			break;
		case '2':
			draw2(g);
			break;
		case '3':
			draw3(g);
			break;
		case '4':
			draw4(g);
			break;
		case '5':
			draw5(g);
			break;
		case '6':
			draw6(g);
			break;
		case '7':
			draw7(g);
			break;
		case '8':
			draw8(g);
			break;
		case '9':
			draw9(g);
			break;
		default:
			g.fillRect(50, 80, 100, 20);	// middle line
		}
	}
	
	public void draw0(Graphics g)
	{
		g.fillRect(50, 0, 20, 90);	// top left line
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(130, 90, 20, 90); // bottom right line
		g.fillRect(50, 160, 100, 20);	// bottom line
		g.fillRect(50, 90, 20, 90);		// bottom left line
	}
	
	public void draw1(Graphics g)
	{
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(130, 90, 20, 90); // bottom right line
	}
	
	public void draw2(Graphics g)
	{
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(50, 80, 100, 20);	// middle line
		g.fillRect(50, 90, 20, 90);		// bottom left line
		g.fillRect(50, 160, 100, 20);	// bottom line
	}
	
	public void draw3(Graphics g)
	{
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(50, 80, 100, 20);	// middle line
		g.fillRect(130, 90, 20, 90); // bottom right line
		g.fillRect(50, 160, 100, 20);	// bottom line
	}
	
	public void draw4(Graphics g)
	{
		g.fillRect(50, 0, 20, 90);	// top left line
		g.fillRect(50, 80, 100, 20);	// middle line
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(130, 90, 20, 90); // bottom right line
	}
	
	public void draw5(Graphics g)
	{
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(50, 0, 20, 90);	// top left line
		g.fillRect(50, 80, 100, 20);	// middle line
		g.fillRect(130, 90, 20, 90); // bottom right line
		g.fillRect(50, 160, 100, 20);	// bottom line
	}
	
	public void draw6(Graphics g)
	{
		g.fillRect(50, 80, 100, 20);	// middle line
		g.fillRect(50, 0, 20, 90);	// top left line
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(130, 90, 20, 90); // bottom right line
		g.fillRect(50, 160, 100, 20);	// bottom line
		g.fillRect(50, 90, 20, 90);		// bottom left line
	}
	
	public void draw7(Graphics g)
	{
		g.fillRect(50, 0, 20, 90);	// top left line
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(130, 90, 20, 90); // bottom right line
	}
	
	public void draw8(Graphics g)
	{
		g.fillRect(50, 80, 100, 20);	// middle line
		g.fillRect(50, 0, 20, 90);	// top left line
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(130, 90, 20, 90); // bottom right line
		g.fillRect(50, 160, 100, 20);	// bottom line
		g.fillRect(50, 90, 20, 90);		// bottom left line
	}
	
	public void draw9(Graphics g)
	{
		g.fillRect(50, 80, 100, 20);	// middle line
		g.fillRect(50, 0, 20, 90);	// top left line
		g.fillRect(50, 0, 100, 20); // top line
		g.fillRect(130, 0, 20, 90);	// top right line
		g.fillRect(130, 90, 20, 90); // bottom right line
		g.fillRect(50, 160, 100, 20);	// bottom line
	}
}
