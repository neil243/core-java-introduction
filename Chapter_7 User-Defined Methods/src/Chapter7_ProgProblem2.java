//********************************************************************
// Author: Neil Kasanda
//
// Program:
// Write a program that prompts the user to input a sequence of characters
// and outputs the number of vowels. (Use the method isVowel written in
// Programming Exercise 1.)
//********************************************************************

import java.util.*;

public class Chapter7_ProgProblem2 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		String input;
		int noOfVowels;
		
		System.out.print("Enter a string to count the number vowels in it (* to quit): ");
		input = console.nextLine();
		System.out.println();
		
		while(!input.equals("*"))
		{
			noOfVowels = countVowels(input);
			System.out.println("Vowel(s) in \"" + input + "\": " + noOfVowels);
			
			System.out.print("Enter a string to count the number vowels in it (* to quit): ");
			input = console.nextLine();
			System.out.println();
		}
		
		System.out.println("Goodbye!");
	}
	
	public static boolean isVowel(char ch)
	{
		switch(ch)
		{
		case 'a':
		case 'A':
		case 'e':
		case 'E':
		case 'i':
		case 'I':
		case 'o':
		case 'O':
		case 'u':
		case 'U':
		case 'y':
		case 'Y':
			return true;
		default:
			return false;
		}
	}
	
	public static int countVowels(String str)
	{
		int count = 0;
		
		for(int i = 0; i < str.length(); i++)
		{
			if(isVowel(str.charAt(i)))
				count++;
		}
		
		return count;
	}
}
