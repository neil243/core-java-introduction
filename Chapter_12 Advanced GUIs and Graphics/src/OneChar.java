//*****************************************************************
// Author: Neil Kasanda
// 
// Program:
// to illustrate key events.
//*****************************************************************

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class OneChar extends JApplet
{
	private JTextField oneLetter = new JTextField(1);
	
	public void init()
	{
		Container c = getContentPane();
		
			// register the listener object
		oneLetter.addKeyListener(new KeyAdapter()
				{
					public void keyTyped(KeyEvent e)
					{
						float red, green, blue;
						
						Color fg, bg;
						
						red = (float)(Math.random());
						green = (float)(Math.random());
						blue = (float)(Math.random());
						
						fg = new Color(red, green, blue);
						bg = Color.white;
						
						oneLetter.setText(" ");
						oneLetter.setForeground(fg);
						oneLetter.setBackground(bg);
						oneLetter.setCaretColor(bg);
						oneLetter.setFont(new Font("Courrier", Font.BOLD, 200));
					}
				}
		);
		
		c.setLayout(new GridLayout(1, 1));
		c.setBackground(Color.white);
		c.add(oneLetter);
		
		JOptionPane.showMessageDialog(null, "Click on the applet, then type a key ",
				"Information", JOptionPane.PLAIN_MESSAGE);
		
	}
}
