//****************************************************************************
// Author: Neil Kasanda
//
// Program implementing the GUI version of the candy machine.
//****************************************************************************

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CandyMachineGUI extends JFrame 
{
	private JLabel headingMainL, selectionL;
	
	private JButton candyB, chipsB, gumB, cookiesB, exitB;
	
	private final int WIDTH = 300;
	private final int HEIGHT = 300;
	
	private CashRegister cashRegister = new CashRegister();
	private Dispenser candy = new Dispenser(2, 50);
	private Dispenser chips = new Dispenser(100, 65);
	private Dispenser gum = new Dispenser(75, 45);
	private Dispenser cookies = new Dispenser(100, 85);
	
	public CandyMachineGUI()
	{
			// Set window title
		setTitle("Candy Machine");
		
			// Instantiate the GUI components
				//Labels
		headingMainL = new JLabel("WELCOME TO SHELLY'S CANDY "
				+ "SHOP", SwingConstants.CENTER);
		selectionL = new JLabel("To Make a Selection, Click "
				+ "on the Product Button", SwingConstants.CENTER);
		
				// Buttons
		candyB = new JButton("Candy");
		chipsB = new JButton("Chips");
		gumB = new JButton("Gum");
		cookiesB = new JButton("Cookies");
		exitB = new JButton("Exit");
		
			// Register action listeners
		ButtonHandler pbHandler = new ButtonHandler();
		candyB.addActionListener(pbHandler);
		chipsB.addActionListener(pbHandler);
		gumB.addActionListener(pbHandler);
		cookiesB.addActionListener(pbHandler);
		exitB.addActionListener(pbHandler);
		
			// Get window pane
		Container pane = getContentPane();
		pane.setLayout(new GridLayout(7, 1));
		
			// Add GUIs to window pane
		pane.add(headingMainL);
		pane.add(selectionL);
		pane.add(candyB);
		pane.add(chipsB);
		pane.add(gumB);
		pane.add(cookiesB);
		pane.add(exitB);
		
			// Finish window setup
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	private class ButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(e.getActionCommand().equals("Exit"))
				System.exit(0);
			else if(e.getActionCommand().equals("Candy"))
				sellProduct(candy, "Candy");
			else if(e.getActionCommand().equals("Chips"))
				sellProduct(chips, "Chips");
			else if(e.getActionCommand().equals("Gum"))
				sellProduct(gum, "Gum");
			else if(e.getActionCommand().equals("Cookies"))
				sellProduct(cookies, "Cookies");
		}
	}
	
	public void sellProduct(Dispenser product, String productName)
	{
		int price = product.getProductCost();
		int coinsRequired = price;
		int coinsInserted = 0;
		String str;
		
		if(product.getCount() > 0)
		{
			while(coinsRequired > 0)
			{
				str = JOptionPane.showInputDialog("To buy " + productName 
						+ " please insert " + coinsRequired + " cents.");
				
				coinsInserted = coinsInserted + Integer.parseInt(str);
				coinsRequired = price - coinsInserted;
			}
			
			cashRegister.acceptAmount(coinsInserted);
			product.makeSale();
			
			JOptionPane.showMessageDialog(null, "Please pick up your " 
					+ productName + " and enjoy", "Thank you, Come again!", 
					JOptionPane.PLAIN_MESSAGE);
		}
		
		else
			JOptionPane.showMessageDialog(null, "Sorry " + productName 
					+ " is sold out\nMake another selection", "Thank you, "
					+ "Come again!", JOptionPane.PLAIN_MESSAGE);
	}
	
	public static void main(String[] args)
	{
		CandyMachineGUI candyMachine = new CandyMachineGUI();
	}
}
