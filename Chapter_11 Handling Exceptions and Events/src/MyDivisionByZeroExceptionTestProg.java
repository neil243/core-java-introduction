//**********************************************************************
// Author: Neil Kasanda
//
// Program: This class uses the class MyDivisionByZeroException.
//**********************************************************************

import java.util.*;

public class MyDivisionByZeroExceptionTestProg 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		double numerator;
		double denominator;
		
		try
		{
			System.out.print("Line 4: Enter the numerator: ");
			numerator = console.nextDouble();
			System.out.println();
			
			System.out.print("Line 7: Enter the denominator: ");
			denominator = console.nextDouble();
			System.out.println();
			
			if(denominator == 0)
				throw new MyDivisionByZeroException();
			
			System.out.println("Line 12: Quotient = " + (numerator / denominator));
		}
		catch(MyDivisionByZeroException mdbze)
		{
			System.out.println("Line 14: " + mdbze.toString());
		}
		catch(Exception e)
		{
			System.out.println("Line 16: " + e.toString());
		}
		
		console.close();
	}
}
