//*******************************************************
// Author: Neil Kasanda
// 
// Program: Create the multiplication table of 1 through 5
//*******************************************************
public class Example5_20 
{
	public static void main(String[] args)
	{
		int i, j;
		
		for(i = 1; i <= 5; i++)
		{
			for(j = 1; j <= 10; j++)
				System.out.printf("%-3d ", i * j);
			System.out.println();
		}
	}
}
