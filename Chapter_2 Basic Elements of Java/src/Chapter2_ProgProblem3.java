//********************************************************
// Author: Neil Kasanda
//
// Problem:
// Repeat Exercise 2 by declaring num1, num2, and num3, and average of type
// 		double. Store 75.35 into num1, -35.56 into num2, and 15.76 into num3.
//********************************************************
public class Chapter2_ProgProblem3 
{
	public static void main(String[] args) 
	{
		double num1;
		double num2;
		double num3;
		double average;
		
		num1 = 75.35;
		num2 = -35.56;
		num3 = 15.76;
		
		average = (num1 + num2 + num3) / 3;
		
		System.out.println("num1 = " + num1);
		System.out.println("num2 = " + num2);
		System.out.println("num3 = " + num3);
		System.out.println("average = " + average);
	}
}
