//*****************************************************************************
// Author: Neil Kasanda
// 
// Program:
// Write a value-returning method, isVowel, that returns the value true if a
// given character is a vowel, and otherwise returns false. Also write a
// program to test your method.
//*****************************************************************************

import java.util.*;

public class Chapter7_ProgProblem1 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		char input;
		System.out.print("Enter a letter to see if it is a vowel (* to quit): ");
		input = console.next().charAt(0);
		System.out.println();
		
		while(input != '*')
		{
			if(isVowel(input))
				System.out.println(input + " is a vowel.");
			else
				System.out.println(input + " is not a vowel");
			
			System.out.print("Enter a letter to see if it is a vowel (* to quit): ");
			input = console.next().charAt(0);
			System.out.println();
		}
	}
	
	public static boolean isVowel(char ch)
	{
		switch(ch)
		{
		case 'a':
		case 'A':
		case 'e':
		case 'E':
		case 'i':
		case 'I':
		case 'o':
		case 'O':
		case 'u':
		case 'U':
		case 'y':
		case 'Y':
			return true;
		default:
			return false;
		}
	}
}
