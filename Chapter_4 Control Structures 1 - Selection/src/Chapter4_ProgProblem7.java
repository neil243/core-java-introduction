//*************************************************************************
// Author: Neil Kasanda
// 
// Program:
// A box of cookies can hold 24 cookies and a container can hold 75 boxes
// of cookies. Write a program that prompts the user to enter the total
// number of cookies. The program then outputs the number of boxes and
// the number of containers to ship the cookies. Note that each box must
// contain the specified number of cookies and each container must contain
// the specified number of boxes. If the last box of cookies contains less
// than the number of specified cookies, you can discard it, and output
// the number of leftover cookies. Similarly, if the last container contains
// less than the number of specified boxes, you can discard it, and output
// the number of leftover boxes.
//*************************************************************************

import java.util.*;

public class Chapter4_ProgProblem7 
{
	static Scanner console = new Scanner(System.in);
	
	static final int COOKIES_PER_BOX = 24;
	static final int BOXES_PER_CONTAINER = 75;
	
	public static void main(String[] args) 
	{
		int noOfCookies;
		int noOfBoxes;
		int noOfContainers;
		
		System.out.print("Enter the total number of cookies: ");
		noOfCookies = console.nextInt();
		System.out.println();
		
		noOfBoxes = noOfCookies / COOKIES_PER_BOX;
		
		noOfContainers = noOfBoxes / BOXES_PER_CONTAINER;
		
		System.out.println("Number of cookie boxes: " + noOfBoxes);
		System.out.println("Number of containers to ship boxes: " + noOfContainers);
		
		if(noOfCookies % COOKIES_PER_BOX > 0)
			System.out.println("Number of leftover cookies: " + 
					(noOfCookies % COOKIES_PER_BOX));
		
		if(noOfBoxes % BOXES_PER_CONTAINER > 0)
			System.out.println("Number of leftover boxes: " + 
					(noOfBoxes % BOXES_PER_CONTAINER));
	}
	
}
