//****************************************************************************
// Author: Neil Kasanda
// 
// Program:
// Palindrome 
//****************************************************************************

import java.util.*;

public class PalindromeProgram 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		String inputString;
		
		System.out.print("Enter a string to see if it is a palindrome: ");
		inputString = console.next();
		System.out.println();
		
		System.out.println("Is " + inputString + 
				" a palindrome: " + isPalindrome(inputString));
	}
	
	public static boolean isPalindrome(String str)
	{
		int j = str.length() - 1; // j is position of last element of str
		
		for(int i = 0; i < str.length() / 2; i++)
		{
			if(Character.toLowerCase(str.charAt(i)) != 
					Character.toLowerCase(str.charAt(j)))
				return false;
			j--;
		}
		
		return true;
	}
}
