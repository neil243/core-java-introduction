//****************************************************************
// Author: Neil Kasanda
//
// Program: Chapter 10, Programming Problem 1
// In Chapter 8, the class Clock was designed to implement the time of day in a
// program. Certain applications, in addition to hours, minutes, and seconds, might
// require you to store the time zone. Derive the class ExtClock from the class
// Clock by adding a data member to store the time zone. Add the necessary
// methods and constructors to make the class functional. Also, write the definitions
// of the methods and the constructors. Finally,write a test program to test your class.
//****************************************************************

public class Chapter10_ProgProblem1_ExtClock extends Clock 
{
	private String timeZone;
	
		// Default Constructor
	public Chapter10_ProgProblem1_ExtClock()
	{
		super();
		timeZone = "";
	}
	
		// Constructor with parameters
	public Chapter10_ProgProblem1_ExtClock(int hours, int minutes, 
			int seconds, String tZone)
	{
		super(hours, minutes, seconds);
		timeZone = tZone;
	}
	
		// Method to set the time
	public void setTime(int hours, int minutes, 
			int seconds, String tZone)
	{
		super.setTime(hours, minutes, seconds);
		timeZone = tZone;
	}
	
	public void setTimeZone(String tZone)
	{
		timeZone = tZone;
	}
	
	public String getTimeZone()
	{
		return timeZone;
	}
	
	public void makeCopy(Chapter10_ProgProblem1_ExtClock otherClock)
	{
		super.makeCopy(otherClock);
		
		timeZone = otherClock.timeZone;
	}
	
	public Chapter10_ProgProblem1_ExtClock getCopy()
	{
		Chapter10_ProgProblem1_ExtClock temp = 
				new Chapter10_ProgProblem1_ExtClock();
		
		temp.setTime(getHours(), getMinutes(), getSeconds(), timeZone);
		
		return temp;
	}
	
		// Method to return the time as a string.
	public String toString()
	{
		if(timeZone.equals(""))
			return super.toString();
		
		return super.toString() + " (" + timeZone + ")";
	}
}
