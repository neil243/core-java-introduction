
public class PersonalInfo 
{
	private Person name;
	private Date bDay;
	private int personID;
	
		// Default constructor
		// Instance variables are set to the default values
		// Postcondition: firstName = ""; lastName = "";
		//				  dMonth = 1; dDay = 1; dYear = 1900;
		//				  personID = 0;
	public PersonalInfo()
	{
		name = new Person();
		bDay = new Date();
		personID = 0;
	}
	
		// Constructor with parameters
		// Instance variables are set according to the parameters
		// Postcondition: firstName = first; lastName = last;
		//				  dMonth = month; dDay = day; dYear = year;
		//				  personID = ID;
	public PersonalInfo(String first, String last, 
			int month, int day, int year, int ID)
	{
		name = new Person(first, last);		// Instantiate and 
											// initialize the object name
		bDay = new Date(month, day, year);	// instantiate and 
											// initialize the object bDay
		personID = ID;
	}
	
		// Method to set the personal information
		// Instance variables are set according to the parameters
		// Postcondition: firstName = first; lastName = last;
		//				  dMonth = month; dDay = day; dYear = year;
		//				  personID = ID;
	public void setPersonalInfo(String first, String last, 
			int month, int day, int year, int ID)
	{
		name.setName(first, last);
		bDay.setDate(month, day, year);
		personID = ID;
	}
	
	public String toString()
	{
		return "Name: " + name
				+ "\nDate of birth: " + bDay
				+ "\nPersonal ID: " + personID;
	}
	
	public static void main(String[] args)
	{
		PersonalInfo idCard1 = new PersonalInfo();
		PersonalInfo idCard2 = new PersonalInfo("Madison", "Clark", 
				6, 26, 1964, 123456789);
		
		System.out.println("Line 4: idCard1: " + idCard1);
		System.out.println();
		System.out.println("Line 6: idCard2: " + idCard2);
		System.out.println();
		
		System.out.println("Line 7: idCard1...");
		idCard1.setPersonalInfo("Nick", "Clark", 5, 22, 1990, 987321654);
		System.out.println("Line 9: idCard1: " + idCard1);
	}
}
