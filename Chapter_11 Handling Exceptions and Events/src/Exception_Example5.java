//**********************************************************************
// Author: Neil Kasanda
//
// Program:
// To illustrate handling of exceptions
//**********************************************************************

import java.util.*;

public class Exception_Example5 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		int dividend, divisor, quotient;
		
		try
		{
			System.out.print("Line 4: Enter the dividend: ");
			dividend = console.nextInt();
			System.out.println();
			
			System.out.print("Line 7: Enter the divisor: ");
			divisor = console.nextInt();
			System.out.println();
			
			quotient = dividend / divisor;
			System.out.println("Line 11: Quotient = " + quotient);
		}
		catch(Exception eRef)
		{
			if(eRef instanceof ArithmeticException)
				System.out.println("Line 14: Exception " + eRef.toString());
			
			else if(eRef instanceof InputMismatchException)
				System.out.println("Line 16: Exception " + eRef.toString());
		}
		
		console.close();
	}
}
