//******************************************************************
// Author: Neil Kasanda
// 
// Program:
// Write a GUI program that converts seconds to years, weeks, days, hours, and
// minutes. For this problem, assume 1 year is 365 days.
//******************************************************************

import javax.swing.*;			// For JFrame and other GUIs
import java.awt.*;				// For Container class
import java.awt.event.*;		// For events

public class Chapter6_ProgProblem2 extends JFrame 
{
	private JLabel inputL, yearL, weekL, dayL, hourL, minuteL;
	
	private JTextField inputTF, yearTF, weekTF, dayTF, hourTF, minuteTF;
	
	private JButton convertB, exitB;
	
	private final int WIDTH = 600;
	private final int HEIGHT = 500;
	
	private final int SECONDS_PER_YEAR = 60 * 60 * 24 * 365;
	private final int SECONDS_PER_WEEK = 60 * 60 * 24 * 7;
	private final int SECONDS_PER_DAY = 60 * 60 * 24;
	private final int SECONDS_PER_HOUR = 60 * 60;
	private final int SECONDS_PER_MINUTE = 60;
	
	public Chapter6_ProgProblem2()
	{
		setTitle("Time Conversion Program");
		
			// Instantiate all the GUIs
		inputL = new JLabel("Enter the number of seconds for conversion: ", 
							SwingConstants.RIGHT);
		yearL = new JLabel("Year(s): ", SwingConstants.RIGHT);
		weekL = new JLabel("Week(s): ", SwingConstants.RIGHT);
		dayL = new JLabel("Day(s): ", SwingConstants.RIGHT);
		hourL = new JLabel("Hour(s): ", SwingConstants.RIGHT);
		minuteL = new JLabel("Minute(s): ", SwingConstants.RIGHT);
		
		inputTF = new JTextField(20);
		yearTF = new JTextField(20);
		weekTF = new JTextField(20);
		dayTF = new JTextField(20);
		hourTF = new JTextField(20);
		minuteTF = new JTextField(20);
		
		convertB = new JButton("Convert");
		exitB = new JButton("Exit");
		
			// Register needed listeners with appropriate GUIs
		convertB.addActionListener(new ConvertButtonHandler());
		exitB.addActionListener(new ExitButtonHandler());
		
			// Get window pane
		Container pane = getContentPane();
		
			// Set pane layout
		pane.setLayout(new GridLayout(7, 2));
		
			// Add GUIs to pane
		pane.add(inputL);
		pane.add(inputTF);
		pane.add(yearL);
		pane.add(yearTF);
		pane.add(weekL);
		pane.add(weekTF);
		pane.add(dayL);
		pane.add(dayTF);
		pane.add(hourL);
		pane.add(hourTF);
		pane.add(minuteL);
		pane.add(minuteTF);
		pane.add(convertB);
		pane.add(exitB);
			// Finish window setup
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	private class ConvertButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			int seconds, years, weeks, days, hours, minutes;
			
			seconds = Integer.parseInt(inputTF.getText());
			
			years = seconds / SECONDS_PER_YEAR;
			seconds = seconds % SECONDS_PER_YEAR;
			
			weeks = seconds / SECONDS_PER_WEEK;
			seconds = seconds % SECONDS_PER_WEEK;
			
			days = seconds / SECONDS_PER_DAY;
			seconds = seconds % SECONDS_PER_DAY;
			
			hours = seconds / SECONDS_PER_HOUR;
			seconds = seconds % SECONDS_PER_HOUR;
			
			minutes = seconds / SECONDS_PER_MINUTE;
			seconds = seconds % SECONDS_PER_MINUTE; // not really necessary but just i.c
			
			yearTF.setText("" + years);
			weekTF.setText("" + weeks);
			dayTF.setText("" + days);
			hourTF.setText("" + hours);
			minuteTF.setText("" + minutes);
		}
	}
	
	private class ExitButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			System.exit(0);
		}
	}
	
	public static void main(String[] args) 
	{
		Chapter6_ProgProblem2 progObj = new Chapter6_ProgProblem2();
	}
}
