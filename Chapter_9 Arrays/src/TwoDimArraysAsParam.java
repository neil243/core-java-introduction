//*********************************************************
// Author: Neil Kasanda
// 
// Program:
// This program illustrates how two-dimensional arrays are
// passed as parameters to methods.
//*********************************************************

public class TwoDimArraysAsParam 
{
	public static void main(String[] args) 
	{
		int[][] board = {{23, 5, 6, 15, 18},
						 {4, 16, 24, 67, 10},
						 {12, 54, 23, 76, 11},
						 {1, 12, 34, 22, 8},
						 {81, 54, 32, 67, 33},
						 {12, 34, 76, 78, 9}};
		
		TwoDimArraysMethods.printMatrix(board);
		System.out.println();
		
		TwoDimArraysMethods.sumRows(board);
		System.out.println();
		
		TwoDimArraysMethods.largestInRows(board);
	}//end main
}
