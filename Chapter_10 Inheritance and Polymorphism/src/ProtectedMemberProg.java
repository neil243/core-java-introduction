//**********************************************************************
// Author: Neil Kasanda
//
// Program:
// The following program shows how the objects of BClass and DClass work
//**********************************************************************

public class ProtectedMemberProg 
{
	public static void main(String[] args)
	{
		BClass bObject = new BClass();
		DClass dObject = new DClass();
		System.out.println("Line 3: bObject: " + bObject);
		
		System.out.println("Line 4: *** Subclass object");
		dObject.setData('&', 2.5, 7);
		System.out.println("Line 6: dObject: " + dObject);
	}
}
