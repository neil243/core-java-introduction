//********************************************************************
// Author: Neil Kasanda
//
// Program: Chapter 10, Programming Problem 3
// A point in the x-y plane is represented by its x-coordinate and y-coordinate.
// Design the class Point that can store and process a point in the x-y plane.
// You should then perform operations on a point, such as showing the point,
// setting the coordinates of the point, printing the coordinates of the point,
// returning the x-coordinate, and returning the y-coordinate. Also, write a test
// program to test various operations on a point.
//********************************************************************

public class Chapter10_ProgProblem3_Point 
{
	private double xCoord;
	private double yCoord;
	
	public Chapter10_ProgProblem3_Point()
	{
		xCoord = 0;
		xCoord = 0;
	}
	
	public Chapter10_ProgProblem3_Point(double x, double y)
	{
		setCoordinates(x, y);
	}
	
	public void setCoordinates(double x, double y)
	{
		xCoord = x;
		yCoord = y;
	}
	
	public String toString()
	{
		return "(" + xCoord + ", " + yCoord + ")";
	}
	
	public double getXCoordinate()
	{
		return xCoord;
	}
	
	public double getYCoordinate()
	{
		return yCoord;
	}
}
