//************************************************************
// Author: Neil Kasanda
//
// Program:
// Fixing bugs in a program that determines the grade based
// on test score.

import java.util.*;

public class GradeProgramBugFix 
{
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		int testScore;
		System.out.print("Enter the test score: ");
		testScore = console.nextInt();
		System.out.println();
		
		switch(testScore / 10)
		{
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			System.out.println("The grade is F.");
			break;
		
		case 6:
			System.out.println("The grade is D.");
			break;
			
		case 7:
			System.out.println("The grade is C.");
			break;
			
		case 8:
			System.out.println("The grade is B.");
			break;
			
		case 9:
		case 10:
			System.out.println("The grade is A.");
			break;
			
		default:
			System.out.println("Invalid test score.");
		}
	}
}
