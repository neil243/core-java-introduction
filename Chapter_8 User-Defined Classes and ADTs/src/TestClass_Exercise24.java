//**************************************************************************
// Author: Neil Kasanda
// 
// Program: Exercise 24 Chapter 8
// a. Write the definitions of the methods as described in the definition of
// 		the class TestClass.
// b. Write a test program to test various operations of the class
// 		TestClass.
//**************************************************************************

public class TestClass_Exercise24 
{
	private int x;
	private int y;
	
		// Default constructor to initialize
		// the instance variables to 0
	public TestClass_Exercise24()
	{
		x = 0;
		y = 0;
	}
	
		// Constructors with parameters to initialize the
		// instance variables to the values specified by
		// the parameters
		// Postcondition: x = a; y = b;
	TestClass_Exercise24(int a, int b)
	{
		x = a;
		y = b;
	}
	
		// return the sum of the instance variables
	public int sum()
	{
		return x + y;
	}
	
		// print the values of the instance variables
	public void print()
	{
		System.out.println(x + " " + y);
	}
	
	public static void main(String[] args) 
	{
		TestClass_Exercise24 x = new TestClass_Exercise24();
		TestClass_Exercise24 y = new TestClass_Exercise24(10, 10);
		
		x.print();
		System.out.println("Line 4: x.sum() = " + x.sum());
		
		System.out.println();
		
		y.print();
		System.out.println("Line 6: y.sum() = " + y.sum());
	}
}
