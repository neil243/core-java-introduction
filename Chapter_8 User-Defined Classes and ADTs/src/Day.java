//**************************************************************************
// Author: Neil Kasanda
//
// Program: Chapter 8 Programming problem 3
// Design and implement the class Day that implements the day of the week
// in a program. The class Day should store the day, such as Sun for Sunday.
// The program should be able to perform the following operations on an
// object of type Day:
// 		a. Set the day.
// 		b. Print the day.
// 		c. Return the day.
// 		d. Return the next day.
// 		e. Return the previous day.
// 		f. Calculate and return the day by adding certain days to the current day.
// 			For example, if the current day is Monday and we add four days, the day
// 			to be returned is Friday. Similarly, if today is Tuesday and we add 13
// 			days, the day to be returned is Monday.
// 		g. Add the appropriate constructors.
// 		h. Write the definitions of the methods to implement the operations for the
// 			class Day, as defined in a through g.
// 		i. Write a program to test various operations on the class Day.
//**************************************************************************

public class Day 
{
	private String day;
	private int dayNumber;
	
		// Default constructor
		// The instance variable is set to the empty string.
		// Postcondition: day = "";
	public Day()
	{
		day = "";
	}
	
		// Constructor with parameters
		// The instance variable day is set according to the
		// parameter provided by the user.
		// Postcondition: this.day = day;
	public Day(String day)
	{
		setDay(day);
	}
	
		// Method to set the day
		// Postcondition: this.day = day;
	public void setDay(String day)
	{
		switch(day)
		{
		case "Sun":
			this.day = day;
			dayNumber = 0;
			break;
			
		case "Mon":
			this.day = day;
			dayNumber = 1;
			break;
			
		case "Tue":
			this.day = day;
			dayNumber = 2;
			break;
			
		case "Wed":
			this.day = day;
			dayNumber = 3;
			break;
			
		case "Thu":
			this.day = day;
			dayNumber = 4;
			break;
			
		case "Fri":
			this.day = day;
			dayNumber = 5;
			break;
			
		case "Sat":
			this.day = day;
			dayNumber = 6;
			break;
		}
	}
	
		// Method to print the day
		// Postcondition: the value of day is printed
	public void printDay()
	{
		System.out.println("Day is: " + this.day);
	}
	
		// Method to return the day
		// Postcondition: the value of the instance variable 
		// day is returned
	public String getDay()
	{
		return day;
	}
	
		// Method to return the next day
		// If today is Sunday, the value of next day is Monday
		// Postcondition: the next day is calculated from
		// today and its value is returned.
	public String getNextDay()
	{
		String nextDay = "";
		
		switch(day)
		{
		case "Sat":
			nextDay = "Sunday";
			break;
			
		case "Sun":
			nextDay = "Monday";
			break;
			
		case "Mon":
			nextDay = "Tuesday";
			break;
			
		case "Tue":
			nextDay = "Wednesday";
			break;
			
		case "Wed":
			nextDay = "Thursday";
			break;
			
		case "Thu":
			nextDay = "Friday";
			break;
			
		case "Fri":
			nextDay = "Saturday";
			break;
		}
		
		return nextDay;
	}
	
		// Method to return the previous day
		// If today is Wednesday, previous day is Tuesday
		// Postcondition: the previous day is calculated from
		// today and its value is returned.
	public String getPreviousDay()
	{
		String previousDay = "";
		
		switch(day)
		{
		case "Sun":
			previousDay = "Saturday";
			break;
		
		case "Mon":
			previousDay = "Sunday";
			break;
			
		case "Tue":
			previousDay = "Monday";
			break;
			
		case "Wed":
			previousDay = "Tuesday";
			break;
			
		case "Thu":
			previousDay = "Wednesday";
			break;
			
		case "Fri":
			previousDay = "Thursday";
			break;
			
		case "Sat":
			previousDay = "Friday";
			break;
		}
		
		return previousDay;
	}
	
		// Method to calculate a day in the future by
		// adding a certain amount of days to today.
		// If today is Friday, 2 days from today is Sunday.
	public String dayDaysFromToday(int number)
	{
		String calculatedDay = "";
		
		switch((dayNumber + number) % 7)
		{
		case 0:
			calculatedDay = "Sunday";
			break;
			
		case 1:
			calculatedDay = "Monday";
			break;
			
		case 2:
			calculatedDay = "Tuesday";
			break;
			
		case 3:
			calculatedDay = "Wednesday";
			break;
			
		case 4:
			calculatedDay = "Thursday";
			break;
			
		case 5:
			calculatedDay = "Friday";
			break;
			
		case 6:
			calculatedDay = "Saturday";
			break;
		}
		
		return calculatedDay;
	}
	
	public static void main(String[] args)
	{
		Day firstDay = new Day();
		Day secondDay = new Day("Tue");
		
		System.out.print("Line 3: firstDay: ");
		firstDay.printDay();
		System.out.println();
		
		System.out.print("Line 6: secondDay: ");
		secondDay.printDay();
		System.out.println();
		
		System.out.println("Line 9: For firstDay...");
		firstDay.setDay("Mon");
		System.out.print("Line 11: After setting firstDay: ");
		firstDay.printDay();
		System.out.println("Line 13: Next day: " + firstDay.getNextDay());
		System.out.println("Line 14: Previous day: " + firstDay.getPreviousDay());
		System.out.println("Line 15: 4 days from today: " + firstDay.dayDaysFromToday(4));
		System.out.println();
		
		System.out.println("Line 17: for secondDay...");
		secondDay.printDay();
		System.out.println("Line 19: Next day: " + secondDay.getNextDay());
		System.out.println("Line 20: Previous day: " + secondDay.getPreviousDay());
		System.out.println("Line 21: 13 days from today: " + secondDay.dayDaysFromToday(13));
		System.out.println("Line 22: Setting secondDay to an invalid value...");
		secondDay.setDay("InvalidDay");
		secondDay.printDay();
		
	}
}
