//*****************************************************************
// Author: Neil Kasanda
// 
// Program:
// The roots of the quadratic equation ax^2 + bx + c = 0, a != 0 are given by the
// following formula: (-b +- sqrt(b^2 - 4ac)) / (2a)
// In this formula, the term b2 - 4ac is called the discriminant. If b2 - 4ac = 0,
// then the equation has a single (repeated) root. If b2 - 4ac > 0, the equation
// has two real roots. If b2 - 4ac < 0, the equation has two complex roots.
// Write a program that prompts the user to input the value of a (the
// coefficient of x2), b (the coefficient of x), and c (the constant term), and
// outputs the type of roots of the equation. Furthermore, if b2 - 4ac >= 0, the
// program should output the roots of the quadratic equation. (Hint: Use the
// method pow or sqrt from the class Math to calculate the square root.
// Chapter 3 explains how to use these methods.)
//*****************************************************************

import java.util.*;

public class Chapter4_ProgProblem8 
{
	static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		double a;
		double b;
		double c;
		
		System.out.print("Enter the value of a: ");
		a = console.nextDouble();
		System.out.println();
		
		System.out.print("Enter the value of b: ");
		b = console.nextDouble();
		System.out.println();
		
		System.out.print("Enter the value of c: ");
		c = console.nextDouble();
		System.out.println();
		
		if((Math.pow(b, 2.0) - 4 * a * c) == 0)
		{
			double root;
			System.out.println("The equation has a single repeated root.");
			
			root = (-b + Math.sqrt(b * b - 4 * a * c)) / (2 * a);
			
			System.out.printf("x = %.4f", root);
		}
		
		else if( (Math.pow(b, 2.0) - 4 * a * c) > 0 )
		{
			double root1, root2;
			System.out.println("The equation has two real roots.");
			
			root1 = (-b + Math.sqrt(b * b - 4 * a * c)) / (2 * a);
			root2 = (-b - Math.sqrt(b * b - 4 * a * c)) / (2 * a);
			
			System.out.printf("x = %.4f or x = %.4f", root1, root2);
		}
		
		else
		{
			System.out.println("The equation has two complex roots.");
		}
	}
}
