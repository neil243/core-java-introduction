//********************************************
// Author: Neil Kasanda
//
// Program: Chapter 10, Programming Problem 4
// testing the various operations of a circle
//********************************************

public class Chapter10_TestProgCircle 
{
	public static void main(String[] args) 
	{
		Chapter10_ProgProblem4_Circle circle = 
				new Chapter10_ProgProblem4_Circle();
		
		System.out.println("Line 7: circle: \n" + circle);
		System.out.println();
		
		circle.setDimensions(5, 8, 13);
		System.out.println("Line 9: After setting circle's dimensions...");
		System.out.println("Line 9: circle: \n" + circle);
	}
}
