//**********************************************************************
// Author: Neil Kasanda
// 
// Program: extended definition of class Person to illustrate cascading
// method calls
//**********************************************************************

public class Person2 
{
	private String firstName;		// store the first name
	private String lastName;		// store the last name
	
		// Default constructor
		// Initialize first and lastName to empty string.
		// Postcondition: firstName = first; lastName = last;
	public Person2()
	{
		firstName = "";
		lastName = "";
	}
	
		// Constructor with parameters
		// Set firstName and lastName according to the parameters.
		// Postcondition: firstName = first; lastName = last;
	public Person2(String first, String last)
	{
		firstName = first;
		lastName = last;
	}
	
		// Method to return the first name and last name
		// in the form firstName lastName
	public String toString()
	{
		return firstName + " " + lastName;
	}
	
		// Method to set firstName and lastName according to
		// the parameters.
		// Postcondition: firstName = first; lastName = last;
	public void setName(String first, String last)
	{
		firstName = first;
		lastName = last;
	}
	
		// Method to set the last name
		// Postcondition: lastName = last;
		//		After setting the last name, a reference
		//		of the object is returned.
	public Person2 setLastName(String last)
	{
		lastName = last;
		return this;
	}
	
		// Method to set the first name
		// Postcondition: firstName = first;
		//		After setting the first name, a reference
		//		of the object is returned.
	public Person2 setFirstName(String first)
	{
		firstName = first;
		return this;
	}
	
		// Method to return the first name
		// Postcondition: the value of firstName is returned.
	public String getFirstName()
	{
		return firstName;
	}
	
		// Method to return the last name
		// Postcondition: the value of lastName is returned
	public String getLastName()
	{
		return lastName;
	}
}