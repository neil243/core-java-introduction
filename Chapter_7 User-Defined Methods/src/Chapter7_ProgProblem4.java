//*******************************************************************
// Author: Neil Kasanda
// 
// Program:
// 4. Consider the following program segment:
// ...
// a. Write the definition of method one so that it returns the sum of
// 		x and y if x is greater than y; otherwise, it should return x minus 2
// 		times y.
// b. Write the definition of method two as follows:
// 		i. Read a number and store it in z.
// 		ii. Update the value of z by adding the value of a to its previous value.
// 		iii. Assign the variable first the value returned by method one with
// 			the parameters 6 and 8.
// 		iv. Update the value of first by adding the value of x to its previous
// 			value.
// 		v. If the value of z is more than twice the value of first, return z;
// 			otherwise, return 2 times first minus z.
// c. Write a Java program that tests parts a and b. (Declare additional variables
// 		in the method main, if necessary.)
//*******************************************************************

import java.util.*;

public class Chapter7_ProgProblem4 
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		int num;
		int num2;
		double dec;
		
		num = 5;
		num2 = 6;
		System.out.println("one(" + num + ", " + num2 + "): " + one(num, num2));
		System.out.println("one(" + num2 + ", " + num + "): " + one(num2, num));
		
		num = 100;
		dec = 12.0;
		System.out.println("two(" + num + ", " + dec + "): " + two(num, dec));
		
		num = 10;
		dec = 12.0;
		System.out.println("two(" + num + ", " + dec + "): " + two(num, dec));
	}
	
	public static int one(int x, int y)
	{
		if(x > y)
			return x + y;
		else
			return x - 2 * y;
	}
	
	public static double two(int x, double a)
	{
		int first;
		double z;
		
		System.out.print("Enter a number: ");
		z = console.nextDouble();
		System.out.println();
		
		z = z + a;
		
		first = one(6, 8);
		first = first + x;
		
		if(z > 2 * first)
			return z;
		else
			return 2 * first - z;
	}
}
